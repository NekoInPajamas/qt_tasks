/*
В строке убрать все цифры, посчитать их количество и среднее арифметическое. Все заглавные буквы заменить на строчные.
*/

#include <iostream>
#include <cstring>

using namespace std;

int main()
{
    char s[32];
    cin.getline(s,32); //Строка считывается со всеми пробелами
    int n = strlen(s); //Длина строки
    int num = 0; //Количество цифр в строке
    int sum = 0; //Сумма цифр в строке
    int simbol = 0; //Код символа строки в таблице ASCII

    for (int i = 0; i < n; ++i)
    {
        simbol = (int)s[i];
        if (simbol >=48 && simbol <=57)
        {
            s[i] = (char)32; //Все цифры в строке заменяем пробелами
            ++num;
            sum += simbol;
        }
    }

    cout << "Строка без цифр: " << s << endl;
    cout << "Количество цифр в строке: " << num << endl;
    cout << "Среднее арифметическое удалённых цифр: " << (char)(sum/num) << endl;

    for (int i = 0; i < n; ++i)
    {
        simbol = (int)s[i];
        if (simbol >=65 && simbol <=90)
        {
            s[i] = (char)(simbol + 32); //Все символы в верхнем регистре заменяем на те же символы в нижнем регистре
        }
    }

    cout << "Строка в нижнем регистре: " << s << endl;

    return 0;
}

