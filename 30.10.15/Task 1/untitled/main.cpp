/*
Посчитать в строке длину максимальной последовательности из единиц, найти позицию символа, с которого она начинается.
Вывести длину, позицию и под исходной строкой на этой позиции вывести символ '^', например, так:
abc ds00114353 cvvbcv1111000
                     ^
или:
abc ds00114353 cvvbcv1111000
_____________________^
*/

#include <iostream>
#include <cstring>

using namespace std;

int main()
{
    char s[64];
    cin.getline(s,64);
    int n = strlen(s);

    int simbol = 0;
    int amount = 0;
    int position = 0;
    int amountMax = 0;
    int positionMax = 1;

    char strSimbol[64];

    while (position < n)
    {
        amount = 0;
        for (int i = position; i < n; ++i)
        {
            simbol = (int)s[i]; //Код символа из таблицы ASCII (у единицы код -- 49)
            if (simbol == 49) //Ищем единицу
            {
                position = i + 1; //Запоминаем позицию следующего элемента
                ++amount; //Длина последовательности единиц равна 1
                break;
            }
        }
        if (amount == 0) //Если единиц в строке не было найдено, завершаем проверку, выходим из цикла while
        {
            position = n;
        }
        for (int i = position; i < n; ++i) //Если единица была найдена, считаем сколько ещё единиц после неё
        {
            simbol = (int)s[i];
            if (simbol == 49)
            {
                ++amount;
                position = i + 1;
            }
            else
            {
                //position = i;
                break;
            }
        }
        if (amount > amountMax) //Ищем максимальную последовательность единиц
        {
            amountMax = amount;
            positionMax = position - amountMax + 1;

        }
    }

    //Создание строки той же длины, что и исходная, в которой все символы будут пустыми, кроме того, с которого начинается
    //максимальная последовательность единиц
    for (int i = 0; i < n; ++i)
    {
        if (i == positionMax - 1)
        {
            strSimbol[i] = '^';
        }
        else
            strSimbol[i] = ' ';
    }

    strSimbol[n]='\0';

    cout << strSimbol << endl;

    cout << "Максимальное количество единиц: " << amountMax << endl;
    cout << "Позиция первого символа из максимальной последовательности единиц: " << positionMax << endl;

    return 0;
}

