#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    //int n = 0;
    char str[32];

    ifstream in("in.txt"); //Открываем файл

    if (!in.is_open()) //Если файл не открылся, выводится сообщение об этом
    {
        cout << "Read file error!" << endl;
        return 1;
    }

    //in >> n >> str; //Считываем данные из файла, записываем в отдельные переменные
    //cout << n << " " << str << endl;
    //in.getline(str,32); //Считываем строку из 32 символов из файла
    //cout << str << endl;

    ofstream out; //Открываем файл
    out.open("out.txt");//, ios::app); //Без опции ios::app всё имеющееся в файле стирается, запись происходит в пустой файл,
                                   //т.е. опция позволяет дописывать в файл

    if (!out.is_open()) //Если файл не открылся, выводится сообщение об этом
    {
        cout << "Write file error!" << endl;
        return 1;
    }

    out << 123 << endl;
    //out << str << endl;

    while (!in.eof()) //Пока не дойдёт до конца файла in.txt, всё будет записываться в файл out.txt
    {
        in.getline(str,32);
        cout << str << endl;
        out << str << endl;
    }

    in.close(); //Закрываем файл
    out.close();

    return 0;
}

