/*
Написать функцию для вычисления определителя матрицы произвольной размерности
способом разложения по элементам строки/столбца, сравнить эффективность с
методом Гаусса.
*/

#include <iostream>
#include <ctime>
#include <cmath>
#include "gaussmthd.h"

using namespace std;

int main()
{
    int n;
    cout << "Введите размерность матрицы" << endl;
    cin >> n;

    double **matrx = matrixGeneration (n); //Генерируется матрица размерности n x n

    printm(matrx, n);

    clock_t start1 = clock(); //Считаем время, потраченное на вычисление определителя

    cout << "Определитель матрицы равен " << detMatrx(matrx, n) << endl;

    clock_t end1 = clock();

    double t1 = (double)(end1 - start1)/CLOCKS_PER_SEC;

    cout << "t1 = " << t1 << endl;

    //-------------------------------------------------------------------------------

    clock_t start2 = clock(); //Считаем время, потраченное на вычисление определителя с использованием метода Гаусса

    //С помощью метода Гаусса матрица приводится к треугольному виду
    gaussMethod(matrx, n);

    printm(matrx, n);

    double det = 1;

    for (int i = 0; i < n; ++i)
    {        
        det *= matrx[i][i]; //Определитель считается как произведение диагональных элементов
    }
    cout << "Определитель матрицы: " << det << endl;

    clock_t end2 = clock();

    double t2 = (double)(end2 - start2)/CLOCKS_PER_SEC;

    cout << "t2 = " << t2 << endl;

    //-------------------------------------------------------------------------------

    for (int i = 0; i < n; ++i)
    {
        delete[] matrx[i];
    }
    delete[] matrx;   

    return 0;
}

