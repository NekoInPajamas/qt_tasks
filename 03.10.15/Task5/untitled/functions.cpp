#include <iostream>
#include <ctime>
#include "functions.h"

using namespace std;

//Функция, выводящая матрицу на экран
void printm (int m, int n, int ** matrix)
{
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
}

//Транспонирование матрицы
void transposition(int m, int n, int ** matrix, int ** matrixTr)
{
    for (int j = 0; j < n; ++j)
    {
        for (int i = 0; i < m; ++i)
        {
            matrixTr[j][i] = matrix[i][j];
        }
    }
}

//Поиск выигрышных цепочек крестиков в главных диагоналях, стоящих за диагональю, начинающейся с элемента [0][0]
bool diagCheck(int m, int n, int k, int ** klPole, bool krestWins)
{
    int decr = 0;
    int krestik;
    int incr;
    int colmn;
    int strg;

    for (int j = 0; j < n; ++j)
    {
        incr = 0;
        strg = 0;
        colmn = j;
        while (colmn < n) //Поскольку "1" могут стоять в разных местах в диагонали и разделяться несколькими "0", проверку
                           //осуществляем, пока не дойдём до конца диагонали
        {
            krestik = 0; //Длина выигрышной цепочки
            incr = colmn - j; //Инкремент для перемещения по столбцам, на которых находятся элементы диагонали
            for (int i = strg; i < m-decr; ++i)
            {
                if (m - decr < k) //Проверяем, не меньше ли длина текущей диагонали длины выигрышной цепочки
                {
                    colmn = n;
                    break;
                }
                else
                {
                    if (klPole[i][j+incr] == 1) //Последовательно просматриваем элементы текущей диагонали, пока не встречаем "1"
                    {
                        ++krestik;
                        colmn = j + incr + 1; //Запоминаем следующий столбец
                        strg = i; //Запоминаем текущую строку
                        break;
                    }
                    ++incr;
                }
            }
            if (krestik == 0) //Если в текущей диагонали нет "1", переходим на следующую диагональ
            {
                colmn = n;
            }
            else
            {
                for (int l = colmn; l < n; ++l) //Если "1" встретилась, считаем длину непрерывной цепочки крестиков
                {
                    if (++strg >= m-decr) //Проверяем, что не вышли за строки
                    {
                        break;
                    }
                    if (klPole[strg][l] == 1)
                    {
                        ++krestik;
                        colmn = l + 1;
                    }
                    else
                    {
                        colmn = l + 1; //Когда встретился "0", проверяем длину получившейся длины цепочки, далее ищем ещё "1"
                                       //в текущей диагонали
                        ++ strg;
                        break;
                    }
                }
                if (krestik >= k) //Если текущая цепочка является выигрышной, выводится сообщение об этом
                {
                    cout << "Крестики выиграли! " << endl;
                    krestWins = true;
                    break;
                }
            }
        }
        if (j >= n - m) //Начиная со столбца (n-m+1), количество строк будет уменьшаться
        {
        ++decr;
        }
    }
    return krestWins;
}

//Поиск выигрышных цепочек крестиков в обратных диагоналях, стоящих до диагонали, начинающейся с элемента [m][0]
bool diagReverse_up(int m, int n, int k, int ** klPole, bool krestWins)
{
    int decr = 0;
    int krestik;
    int incr;
    int colmn;
    int strg;

                                    //В матрице размерности mxm (то же самое, что и nxn, поскольку матрица квадратная) начинаем
                                    //рассматривать обратные диагонали, стоящие до той, которая содержит элементы [0][m-1] и
    for (int j = n-1; j >= 0 ; --j)  //[m-1][0].
    {
        incr = 0;
        strg = 0;
        colmn = j;
        while (colmn >= 0) //Поскольку "1" могут стоять в разных местах в диагонали и разделяться несколькими "0", проверку
                            //осуществляем, пока не дойдём до нижнего элемента диагонали
        {
            krestik = 0; //Длина выигрышной цепочки
            incr = colmn - j; //Инкремент для перемещения по столбцам, на которых находятся элементы диагонали
            for (int i = strg; i < m-decr; ++i)
            {
                if (m - decr < k) //Проверяем, не меньше ли длина текущей диагонали длины выигрышной цепочки
                {
                    colmn = -1;
                    break;
                }
                else
                {
                    if (klPole[i][j-incr] == 1) //Последовательно просматриваем элементы текущей диагонали, пока не встречаем "1"
                    {
                        ++krestik;
                        colmn = j - incr - 1; //Запоминаем предыдущий столбец
                        strg = i; //Запоминаем текущую строку
                        break;
                    }
                    ++incr;
                }
            }
            if (krestik == 0) //Если в текущей диагонали нет "1", переходим на предыдущую диагональ
            {
                colmn = -1;
            }
            else
            {
                for (int l = colmn; l >= 0; --l) //Если "1" встретилась, считаем длину непрерывной цепочки крестиков
                {
                    ++strg;
                    if (klPole[strg][l] == 1)
                    {
                        ++krestik;
                        colmn = l - 1;
                    }
                    else
                    {
                        colmn = l - 1; //Когда встретился "0", проверяем длину получившейся длины цепочки, далее ищем ещё "1"
                                       //в текущей диагонали
                        ++ strg;
                        break;
                    }
                }
                if (krestik >= k) //Если текущая цепочка является выигрышной, выводится сообщение об этом
                {
                    cout << "Крестики выиграли! " << endl;
                    krestWins = true;
                    break;
                }
            }
        }        
        ++decr;        
    }
    return krestWins;
}

//Поиск выигрышных цепочек крестиков в обратных диагоналях, стоящих за диагональю, начинающейся с элемента [m][0]
bool diagReverse_down(int m, int n, int k, int ** klPole, bool krestWins)
{
    int decr = 0;
    int krestik;
    int incr;
    int colmn;
    int strg;

                                //В матрице размерности mxm (то же самое, что и nxn, поскольку матрица квадратная) начинаем
                                //рассматривать обратные диагонали, стоящие за той, которая содержит элементы [0][m-1] и
    for (int j = 0; j < n; ++j) //[m-1][0]
    {
        incr = 0;
        strg = m-1;
        colmn = j;
        while (colmn < n) //Поскольку "1" могут стоять в разных местах в диагонали и разделяться несколькими "0", проверку
                          //осуществляем, пока не дойдём до нижнего элемента диагонали
        {
            krestik = 0; //Длина выигрышной цепочки
            incr = colmn - j; //Инкремент для перемещения по столбцам, на которых находятся элементы диагонали
            for (int i = strg; i >= decr; --i)
            {
                if (m - decr < k) //Проверяем, не меньше ли длина текущей диагонали длины выигрышной цепочки
                {
                    colmn = n;
                    break;
                }
                else
                {
                    if (klPole[i][j+incr] == 1) //Последовательно просматриваем элементы текущей диагонали, пока не встречаем "1"
                    {
                        ++krestik;
                        colmn = j + incr + 1; //Запоминаем следующий столбец
                        strg = i; //Запоминаем текущую строку
                        break;
                    }
                    ++incr;
                }
            }
            if (krestik == 0) //Если в текущей диагонали нет "1", переходим на следующую диагональ
            {
                colmn = n;
            }
            else
            {
                for (int l = colmn; l < n; ++l) //Если "1" встретилась, считаем длину непрерывной цепочки крестиков
                {
                    if (--strg < 0) //Проверяем, что не вышли за строки
                    {
                        break;
                    }
                    if (klPole[strg][l] == 1)
                    {
                        ++krestik;
                        colmn = l + 1;
                    }
                    else
                    {
                        colmn = l + 1; //Когда встретился "0", проверяем длину получившейся длины цепочки, далее ищем ещё "1"
                                       //в текущей диагонали
                        -- strg;
                        break;
                    }
                }
                if (krestik >= k) //Если текущая цепочка является выигрышной, выводится сообщение об этом
                {
                    cout << "Крестики выиграли! " << endl;
                    krestWins = true;
                    break;
                }
            }
        }
        if (j >= n - m)
        {
        ++decr;
        }
    }
    return krestWins;
}


