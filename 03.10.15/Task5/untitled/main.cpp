/*
Клеточное поле размером mxn есть результат игры в крестики-нолики.
Проверить, не закончена ли игра выигрышем «крестиков»? Считается, что крестики
выиграли, если на поле найдется по горизонтали, вертикали или диагонали цепочка,
состоящая подряд из k крестиков.
*/

#include <iostream>
#include <ctime>
#include "functions.h"

using namespace std;

int main()
{
    int m, n, k;
    cout << "Введите размеры клеточного поля" << endl;
    cin >> m >> n;
    cout << "Введите длину выигрышной цепочки" << endl;
    cin >> k;

    srand(time(NULL));

//Генерируем поле из "0"(нолики) и "1"(крестики)
    int ** klPole = new int * [m];
    for (int i = 0; i < m; ++i)
    {
        klPole[i] = new int [n];
        for (int j = 0; j < n; ++j)
        {
            klPole[i][j] = rand()%2;            
        }
    }
    printm (m, n, klPole);

//Создание динамического массива для хранения транспонированной матрицы
        int ** poleTr = new int * [n];
        for (int i = 0; i < n; ++i)
        {
            poleTr[i] = new int [m];
        }


    bool krestWins = false;

//Поиск выигрышных цепочек крестиков в строках
    for (int i = 0; i < m; ++i)
    {
        int colmn = 0;
        while (colmn < n) //Поскольку "1" могут стоять в разных местах в строке и разделяться несколькими "0", проверку осуществляем, пока не дойдём до конца строки
        {
            int krestik = 0;
            for (int j = colmn; j < n; ++j)
            {
                if (klPole[i][j] == 1) //Последовательно просматриваем элементы текущей строки, пока не встречаем "1"
                {
                    ++krestik; //Длина цепочки крестиков становится равной 1
                    colmn = j + 1; //Запоминаем следующий столбец
                    break;
                }
            }
            if (krestik == 0)
            {
                colmn = n; //Если в текущей строке нет "1", переходим на следующую строку
            }
            else
            {
                for (int j = colmn; j < n; ++j) //Если "1" встретилась, считаем длину непрерывной цепочки крестиков
                {
                    if (klPole[i][j] == 1)
                    {
                        ++krestik;
                        colmn = j + 1;
                    }
                    else
                    {
                        colmn = j + 1;
                        break;
                    }
                }
                if (krestik >= k)
                {
                    cout << "Крестики выиграли! " << i << " строка" << endl;
                    krestWins = true;
                    break;
                }
            }
        }
    }

//Поиск выигрышных цепочек крестиков в столбцах
    for (int j = 0; j < n; ++j)
    {
        int strg = 0;
        while (strg < m) //Поскольку "1" могут стоять в разных местах в столбце и разделяться несколькими "0", проверку осуществляем,
                         //пока не дойдём до конца столбца
        {
            int krestik = 0;
            for (int i = strg; i < m; ++i)
            {
                if (klPole[i][j] == 1) //Последовательно просматриваем элементы текущего столбца, пока не встречаем "1"
                {
                    ++krestik; //Длина цепочки крестиков становится равной 1
                    strg = i + 1; //Запоминаем следующую строку
                    break;
                }
            }
            if (krestik == 0)
            {
                strg = m; //Если в текущем столбце нет "1", переходим на следующий столбец
            }
            else
            {
                for (int i = strg; i < m; ++i) //Если "1" встретилась, считаем длину непрерывной цепочки крестиков
                {
                    if (klPole[i][j] == 1)
                    {
                        ++krestik;
                        strg = i + 1;
                    }
                    else
                    {
                        strg = i + 1;
                        break;
                    }
                }
                if (krestik >= k)
                {
                    cout << "Крестики выиграли! " << j << " столбец" << endl;
                    krestWins = true;
                    break;
                }
            }
        }
    }


//Поиск выигрышных цепочек крестиков в диагоналях
    int ** tmpMtrx = new int * [m-1]; //Создание динамического массива для хранения матрицы, содержащей элементы,
                                       //стоящие под диагональю, начинающейся с элемента [0][0], в случае прямых диагоналей
                                       //или стоящие над диагональю, начинающейся с элемента [m-1][0], в случае обратных диагоналей
    int ** tmpMtrxTr = new int * [m-1]; //Динамический массив для хранения транспонированной матрицы
    for (int i = 0; i < m-1; ++i)
    {
        tmpMtrx[i] = new int [m-1];
        tmpMtrxTr[i] = new int [m-1];
    }

    if (m > n) //В случае, когда число строк больше числа столбцов, транспонируем матрицу, задающую клеточное поле, и далее
                //действуем по алгоритму для матрицы, в которой число строк меньше либо равно числу столбцов
    {
        transposition(m, n, klPole, poleTr);

        for (int i = 0; i < m; ++i)
        {
            delete[] klPole[i];
        }
        delete[] klPole;

        klPole = poleTr;

        m = n + m;
        n = m - n;
        m = m - n;
    }

 //Поиск выигрышных цепочек крестиков в диагоналях матрицы, в которой число строк меньше либо равно числу столбцов
    cout << "Прямые диагонали:" << endl;

    krestWins = diagCheck(m, n, k, klPole, krestWins); //Проверка прямых диагоналей, начиная с диагонали, формирующейся
                                                       //с элемента [0][0]
    for (int i = 1; i < m; ++i)
    {
        for (int j = 0; j < m-1; ++j)
        {
            tmpMtrx[i-1][j] = klPole[i][j]; //Формируем матрицу, содержащую элементы, стоящие под диагональю,
                                            //начинающейся с элемента [0][0]
        }
    }

    transposition(m-1, m-1, tmpMtrx, tmpMtrxTr);
    krestWins = diagCheck(m-1, m-1, k, tmpMtrxTr, krestWins); //Проверка прямых диагоналей, стоящих под диагональю,
                                                                //формирующейся с элемента [0][0]
    cout << "Обратные диагонали:" << endl;

    for (int i = 0; i < m-1; ++i)
    {
        for (int j = 0; j < m-1; ++j)
        {
            tmpMtrx[i][j] = klPole[i][j]; //Формируем матрицу, содержащую элементы, стоящие над обратной диагональю,
                                            //начинающейся с элемента [m-1][0]
        }
    }

    cout << "Up" << endl;
    krestWins = diagReverse_up(m-1, m-1, k, tmpMtrx, krestWins); //Проверка обратных диагоналей, стоящих над обратной
                                                                //диагональю, формирующейся с элемента [m-1][0]
    cout << "Down" << endl;
    krestWins = diagReverse_down(m, n, k, klPole, krestWins); //Проверка обратных диагоналей, стоящих после обратной
                                                              //диагонали, формирующейся с элемента [m-1][0]


//Если выигрышные цепочки нигде не встретились, выводится сообщение об этом
    if (krestWins == false)
    {
        cout << "Крестики проиграли!" << endl;
    }


//Удаление места, занимаемого под хранение всех динамических массивов
    for (int i = 0; i < m; ++i)
    {
        delete[] klPole[i];
    }
    delete[] klPole;

    for (int i = 0; i < n; ++i)
    {
        delete[] poleTr[i];
    }
    delete[] poleTr;

    for (int i = 0; i < m-1; ++i)
    {
        delete[] tmpMtrx[i];
        delete[] tmpMtrxTr[i];
    }
    delete[] tmpMtrx;
    delete[] tmpMtrxTr;

    return 0;
}

