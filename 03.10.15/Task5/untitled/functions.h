#ifndef FUNCTIONS
#define FUNCTIONS

void printm (int, int, int **);
void transposition(int, int, int **, int **);
bool diagCheck(int, int, int, int **, bool);
bool diagReverse_up(int, int, int, int **, bool);
bool diagReverse_down(int, int, int, int **, bool);

#endif // FUNCTIONS

