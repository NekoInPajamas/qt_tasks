#include <iostream>
#include <cmath>
#include <ctime>
#include "functions.h"

using namespace std;




int main()
{
    int x1 = 3, y = 4;
    cout << "Before: " << x1 << " " << y << endl;
    swap(x1, y); //Перестановка x1 и y местами
    //swap_pointers(&x1, &y); //Перестановка x1 и y местами через адреса аргументов
    //swap_refs(x1, y); //Перестановка x1 и y местами по ссылкам
    cout << "After: " << x1 << " " << y << endl;

    srand(time(NULL));

    int n = 5;
    double *x = new double [n];
    for (int i = 0; i < n; ++i)
    {
        x[i] = rand()%10;
        cout << x[i] << " ";
    }

    cout << endl;
    cout << avg(x, n) << endl; //Подсчёт среднего арифметического

    delete[] x;

    return 0;
}
