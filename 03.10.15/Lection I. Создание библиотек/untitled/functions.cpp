#include "functions.h"


//Передача аргумента по значению
void swap(int a, int b)
{
    cout << a << " " << b << endl;
    int tmp = a;
    a = b;
    b = tmp;
    cout << a << " " << b << endl;
}

//Передача адресов аргументов
void swap_pointers(int *a, int *b)
{
    cout << *a << " " << *b << endl;
    int tmp = *a;
    *a = *b;
    *b = tmp;
    cout << *a << " " << *b << endl;
}

//Передача аргумента по ссылке
void swap_refs(int &a, int &b)
{
    cout << a << " " << b << endl;
    int tmp = a;
    a = b;
    b = tmp;
    cout << a << " " << b << endl;
}

//Передача массива в качестве аргумента
double avg (double *x, int n)
{
    double sum = 0;
    for (int i = 0; i < n; ++i)
    {
        sum += x[i];
    }
    return sum /= n;
}

