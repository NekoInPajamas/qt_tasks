/*
1) Написать функцию для приведения матрицы произвольной размерности к тре-
угольному виду по методу Гаусса.
2) Написать функцию для вычисления определителя матрицы произвольной раз-
мерности на основе метода Гаусса.
*/

#include <iostream>
#include <ctime>
#include <cmath>
#include "gaussmthd.h"

using namespace std;

int main()
{
    int n;

    cout << "Введите размерность матрицы" << endl;
    cin >> n;

    //Генерируется матрица размерности n x n
    double **matrx = matrixGeneration(n);

    //Вывод на экран сгенерированной матрицы
    cout << "Исходная матрица:" << endl;
    printm(matrx, n);

    //С помощью метода Гаусса матрица приводится к треугольному виду
    int signm = gaussMethod(matrx, n);

    double detMatrx = 1;

    //Подсчёт определителя
    for (int i = 0; i < n; ++i)
    {      
        detMatrx *= matrx[i][i]; //Определитель считается как произведение диагональных элементов
    }
    cout << "Определитель матрицы: " << pow(-1,signm)*detMatrx << endl;

//Удаляем все динамические массивы
    for (int i = 0; i < n; ++i)
    {
        delete[] matrx[i];
    }
    delete[] matrx;

    return 0;
}
