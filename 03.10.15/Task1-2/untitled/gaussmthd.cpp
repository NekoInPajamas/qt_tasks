#include <iostream>
#include <ctime>
#include <cmath>
#include <cstdlib>
#include "gaussmthd.h"

using namespace std;

//Функция, генерирующая матрицу размерности n x n
double** matrixGeneration (int n)
{
    srand(time(NULL));

    double **matrx = new double *[n];
    for (int i = 0; i < n; ++i)
    {
        matrx[i] = new double [n];
        for (int j = 0; j < n; ++j)
        {
            matrx[i][j] = rand()%10;
        }
    }
return matrx;
}


//Функция, выводящая матрицу на экран
void printm(double ** matrx, int n)
{
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            cout << matrx[i][j] << " ";
        }
        cout << endl;
    }
cout << endl;
}


//Функция, приводящая с помощью метода Гаусса матрицу к треугольному виду
int gaussMethod(double **matrx, int n)
{
    int signm = 0;
    int l;

    for (int k = 0; k < n-1; ++k)
    {
        double tmp = matrx[k][k]; //Переменная, хранящая текущий диагональный элемент

        if (tmp == 0) //Если текущий диагональный элемент равен 0,
            {
                for (int i = k + 1; i < n ; ++i)
                {
                    if (matrx[i][k] != 0)
                    {
                        tmp = matrx[i][k]; // ищем в последующих строках ненулевой элемент (в том же столбце, что и диагональный элемент)
                        l = i; //Запоминаем строку, в котором ненулевой элемент находится
                        break;
                    }
                }
                if (tmp != 0)
                {
                    for (int j = 0; j < n; ++j) //Меняем местами строку с текущим диагональным элементом и строку с найденным ненулевым элементом
                    {
                        matrx[k][j] += matrx[l][j];
                        matrx[l][j] = matrx[k][j] - matrx[l][j];
                        matrx[k][j] -= matrx[l][j];
                    }
                    ++ signm; //Была произведена перестановка строк, поэтому знак определителя изменится
                    cout << "Перестановка строк:" << endl;
                    printm(matrx, n);
                }
                else
                {
                    for (int j = k + 1; j < n; ++j) //Если ненулевые элементы в последующих строках не были найдены,
                    {
                        if (matrx[k][j] != 0) //ищем в последующих столбцах ненулевой элемент (в той же строке, что и диагональный элемент)
                        {
                        tmp = matrx[k][j];
                        l = j; //Запоминаем столбец, в котором находится ненулевой элемент
                        break;
                        }
                    }
                    if (tmp != 0)
                    {
                        for (int i = 0; i < n; ++i) //Меняем местами столбец с текущим диагональным элементом и столбец с найденным ненулевым элементом
                        {
                            matrx[i][k] += matrx[i][l];
                            matrx[i][l] = matrx[i][k] - matrx[i][l];
                            matrx[i][k] -= matrx[i][l];
                        }
                        ++ signm; //Была произведена перестановка столбцов, поэтому знак определителя изменится
                        cout << "Перестановка столбцов:" << endl;
                        printm(matrx, n);
                    }
                    else
                    {
                        break; //Если ненулевые элементы в последующих столбцах не были найдены, то матрица не приводится к треугольному виду и определитель будет равен 0
                    }

                }
            }

        //По методу Гаусса обнуляем элементы, стоящие под главной диагональю
        for (int i = k + 1; i < n; ++i)
        {
            double tmp_nxtStrings = matrx[i][k];
            for (int j = k; j < n; ++j)
            {
                matrx[i][j] -= (tmp_nxtStrings/tmp) * matrx[k][j];
            }
            cout << "Обнуляем элемент, стоящий в " << i << " строке, в " << k << " столбце:" << endl;
            printm(matrx, n);
        }
    }


//Округляем достаточно малые элементы (меньше, чем 10^(-12)) матрицы до 0
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            if (abs(matrx[i][j]) < 1e-12 && matrx[i][j] != 0)
            {
                matrx[i][j] = 0;
                cout << "Округление до 0:" << endl;
                printm(matrx, n);
            }
        }
    }  

return signm;
}

