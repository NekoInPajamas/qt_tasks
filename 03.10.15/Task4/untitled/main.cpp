/*
Седловой точкой в матрице называется элемент, являющийся одновременно
наибольшим в столбце и наименьшим в строке. Седловых точек может быть несколько (в
этом случае они имеют равные значения). В матрице А(m,n) найти седловую точку и ее
координаты либо установить, что такой точки нет.
*/

#include <iostream>
#include <ctime>

using namespace std;

int main()
{
    int m, n;
    cout << "Введите размерности матрицы" << endl;
    cin >> m >> n;

    srand(time(NULL));

    //Генерируем матрицу A(m, n)
    double **matrx = new double *[m];
    for (int i = 0; i < m; ++i)
    {
        matrx[i] = new double [n];
        for (int j = 0; j < n; ++j)
        {
            matrx[i][j] = rand()%9+1;
            cout << matrx[i][j] << " ";
        }
        cout << endl;
    }


    double ** minStrEl = new double *[m]; //Матрица для хранения всех минимальных элементов каждой строки (их несколько, если мин. элемент встречается не раз)
    int ** minCol = new int *[m]; //Матрица для хранения столбцов, в которых находятся мин. элементы

    for (int i = 0; i < m; ++i)
    {
        minStrEl[i] = new double [n];
        minCol[i] = new int [n];

        minStrEl[i][0] = matrx[i][0]; //Для начала предположим, что мин. элементом каждой строки являются элементы, стоящие в 0-м столбце
        minCol[i][0] = 0; //Столбцом, в котором находится мин. элемент, является 0-й
        for (int j = 1; j < n; ++j)
        {
            if (minStrEl[i][0] > matrx[i][j]) //Шагая по всем элементам каждой строки, ищем минимальный
            {
                minStrEl[i][0] = matrx[i][j];
                minCol[i][0] = j; //Запоминаем столбец, в котором мин. элемент находится
            }
        }
    }

    int * dimnsn = new int [m]; //Матрица для хранения длин строк матрицы minStrEl

    for (int i = 0; i < m; ++i)
    {
        int k = 0;
        int l = minCol[i][0];
        for (int j = 0; j < l; ++j)
        {
            if (matrx[i][j] == minStrEl[i][0]) //Ищем в каждой строке, есть ли повторения мин. элемента
            {
                ++k;
                minStrEl[i][k] = matrx[i][j];
                minCol[i][k] = j; //Если повторения есть, запоминаем также и номера столбцов, в которых они находятся
            }
        }
        for (int j = l + 1; j < n; ++j)
        {
            if (matrx[i][j] == minStrEl[i][0])
            {
                ++k;
                minStrEl[i][k] = matrx[i][j];
                minCol[i][k] = j;
            }
        }
        dimnsn[i] = k + 1; //Количество мин. элементов в каждой строке
        cout << "Размерность: " << dimnsn[i] << endl;

    }

    //Выводим все мин. элементы с повторениями
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < dimnsn[i]; ++j)
        {
            cout << "Мин. элемент: " << minStrEl[i][j] << " ";
            cout << endl;
        }
    }

    //Выводим все столбцы, в которых находятся мин. элементы и их повторения
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < dimnsn[i]; ++j)
        {
            cout << "Столбец:  " << minCol[i][j] << " ";
            cout << endl;
        }
    }

//Проверяем, является ли мин. элемент строки максимальным в столбце
    int sedlEl = 0;

    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < dimnsn[i]; ++j)
        {
            int maxCol = 0;
            int l = minCol[i][j];
            for (int k = 0; k < i; ++k)
            {
                if (minStrEl[i][j] < matrx[k][l])
                {
                    ++ maxCol;
                }
            }
            for (int k = i + 1; k < m; ++k)
            {
                if (minStrEl[i][j] < matrx[k][l])
                {
                    ++ maxCol;
                }
            }
            if (maxCol == 0)
            {
                cout << "Точка с координатами " << i << ", " << minCol[i][j] << " является седловой." << endl;
                ++ sedlEl;
            }
        }
    }

    if (sedlEl == 0)
    {
        cout << "В матрице нет седловых точек." << endl;
    }

    //Удаляем все матрицы
    for (int i = 0; i < m; ++i)
    {
        delete[] matrx[i];
        delete[] minStrEl[i];
        delete[] minCol[i];
    }
    delete[] matrx;
    delete[] minStrEl;
    delete[] minCol;
    delete[] dimnsn;

    return 0;
}
