#ifndef HEADER
#define HEADER

typedef unsigned short WORD; //Позволяет дать новые имена старым типам данных
typedef unsigned int DWORD;
typedef int LONG;
typedef unsigned char BYTE;

#pragma pack(push,1)
struct BITMAPFILEHEADER
{
    WORD bfType; //Тип файла (первые два байта(символа) -- "BM")
    DWORD bfSize;
    WORD bfReserved1;
    WORD bfReserved2;
    DWORD bfOffBits;
};

struct BITMAPINFOHEADER
{
    DWORD biSize;
    LONG biWidth;
    LONG biHeight;
    WORD biPlanes;
    WORD biBitCount;
    DWORD biCompression;
    DWORD biSizeImage;
    LONG biXPelsPerMeter;
    LONG biYPelsPerMeter;
    DWORD biClrUsed;
    DWORD biClrImportant;
};
#pragma pack(pop)

#endif // HEADER

