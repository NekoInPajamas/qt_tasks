#include <iostream>
#include <fstream>
#include <algorithm>
#include "header.h"

using namespace std;

int main()
{
    ifstream in;
    in.open("kidsnoise.bmp", ios::binary);
    if (!in.is_open())
    {
        cout << "File is not opened!" << endl;
        return 1;
    }

    BITMAPFILEHEADER bmfh;
    BITMAPINFOHEADER bmih;

    //Считываем File Header (информация о самом файле)
//    in.read((char*)&bmfh.bfType, sizeof(WORD)); //Считали первые 2 байта
//    in.read((char*)&bmfh.bfSize, sizeof(DWORD));
//    in.read((char*)&bmfh.bfReserved1, sizeof(WORD));
//    in.read((char*)&bmfh.bfReserved2, sizeof(WORD));
//    in.read((char*)&bmfh.bfOffBits, sizeof(DWORD));
    in.read((char*)&bmfh,sizeof(BITMAPFILEHEADER));

    cout.write((char*)&bmfh.bfType,sizeof(WORD));
    cout << endl;
    cout << "bfSize: " << bmfh.bfSize << endl;
    cout << "bfReserved1: " << bmfh.bfReserved1 << endl;
    cout << "bfReserved2: " << bmfh.bfReserved2 << endl;
    cout << "bfOffBits: " << bmfh.bfOffBits << endl;
    cout << endl;

    //Считываем Info Header (данные о самом изображении)
//    in.read((char*)&bmih.biSize, sizeof(DWORD));
//    in.read((char*)&bmih.biWidth, sizeof(LONG));
//    in.read((char*)&bmih.biHeight, sizeof(LONG));
//    in.read((char*)&bmih.biPlanes, sizeof(WORD));
//    in.read((char*)&bmih.biBitCount, sizeof(WORD));
//    in.read((char*)&bmih.biCompression, sizeof(DWORD));
//    in.read((char*)&bmih.biSizeImage, sizeof(DWORD));
//    in.read((char*)&bmih.biXPelsPerMeter, sizeof(LONG));
//    in.read((char*)&bmih.biYPelsPerMeter, sizeof(LONG));
//    in.read((char*)&bmih.biClrUsed, sizeof(DWORD));
//    in.read((char*)&bmih.biClrImportant, sizeof(DWORD));
    in.read((char*)&bmih,sizeof(BITMAPINFOHEADER));

    cout << "Width: " << bmih.biWidth << endl;
    cout << "Height: " << bmih.biHeight << endl;

//    //Смещаемся к массиву пикселей
//    in.seekg(bmfh.bfOffBits);

    //Создаём трёхмерный динамический массив для записи изображения
    //(указатель на массив указателей на массивы указателей, т.е. здесь будут храниться указатели на строки)
    BYTE *** pix_colors = new BYTE ** [bmih.biHeight];
    BYTE buf[3];

    for (int i = 0; i < bmih.biHeight; ++i)
    {
        pix_colors[i] = new BYTE * [bmih.biWidth]; //Здесь будут храниться указатели на элементы в строках
        for (int j = 0; j < bmih.biWidth; ++j)
        {
            pix_colors[i][j] = new BYTE [3]; //Здесь будут храниться RGB-компоненты
            in.read((char*)pix_colors[i][j],3); //Считываем из файла 3 байта
        }

        //Смещаемся на количество байт (нулей), такое, чтобы длина строки с этими приписанными в конце нулями была кратна 4
        if (3*bmih.biWidth % 4 != 0)
        {
            in.read((char*)buf, 4 - 3*bmih.biWidth % 4);
        }
        //Короткая запись
        //in.seekg(4 - 3*bmih.biWidth % 4, ios::curr);
    }

    //Фильтрация изображения
    for (int i = 1; i < bmih.biHeight-1; ++i)
    {
        for (int j = 1; j < bmih.biWidth-1; ++j)
        {
//            int sum_rgb = 0;
            for (int k = 0; k < 3; ++k)
            {
                BYTE neighbors[9]; //Массив соседних пикселей с текущим пикселем (всего их 9)

                neighbors[0] = pix_colors[i-1][j-1][k];
                neighbors[1] = pix_colors[i-1][j][k];
                neighbors[2] = pix_colors[i-1][j+1][k];

                neighbors[3] = pix_colors[i][j-1][k];
                neighbors[4] = pix_colors[i][j][k];
                neighbors[5] = pix_colors[i][j+1][k];

                neighbors[6] = pix_colors[i+1][j-1][k];
                neighbors[7] = pix_colors[i+1][j][k];
                neighbors[8] = pix_colors[i+1][j+1][k];

                  //Фильтрация размытием
//                int sum = 0;
//                for (int s = 0; s < 9; ++s)
//                {
//                    sum += neighbors[s];
//                }

//                pix_colors[i][j][k] = sum/9;

                //Медианный фильтр
                sort(neighbors, neighbors + 9); //Сортировка значений пикселей по возрастанию
                pix_colors[i][j][k] = neighbors[4]; //Берём средний элемент последовательности (медиана)

                //Чёрно-белое изображение
//                sum_rgb += pix_colors[i][j][k];
            }
//            for (int k = 0; k < 3; ++k)
//            {
//                pix_colors[i][j][k] = sum_rgb/3;
//            }
        }
    }

    //Открываем файл для записи
    ofstream out("result.bmp", ios::binary);
    if (!out.is_open())
    {
        cout << "Out file is not opened!" << endl;
        return 1;
    }

//    out.write((char*)&bmfh.bfType, sizeof(WORD)); //Считали первые 2 байта
//    out.write((char*)&bmfh.bfSize, sizeof(DWORD));
//    out.write((char*)&bmfh.bfReserved1, sizeof(WORD));
//    out.write((char*)&bmfh.bfReserved2, sizeof(WORD));
//    out.write((char*)&bmfh.bfOffBits, sizeof(DWORD));

//    out.write((char*)&bmih.biSize, sizeof(DWORD));
//    out.write((char*)&bmih.biWidth, sizeof(LONG));
//    out.write((char*)&bmih.biHeight, sizeof(LONG));
//    out.write((char*)&bmih.biPlanes, sizeof(WORD));
//    out.write((char*)&bmih.biBitCount, sizeof(WORD));
//    out.write((char*)&bmih.biCompression, sizeof(DWORD));
//    out.write((char*)&bmih.biSizeImage, sizeof(DWORD));
//    out.write((char*)&bmih.biXPelsPerMeter, sizeof(LONG));
//    out.write((char*)&bmih.biYPelsPerMeter, sizeof(LONG));
//    out.write((char*)&bmih.biClrUsed, sizeof(DWORD));
//    out.write((char*)&bmih.biClrImportant, sizeof(DWORD));
    out.write((char*)&bmfh,sizeof(BITMAPFILEHEADER));
    out.write((char*)&bmih,sizeof(BITMAPINFOHEADER));

    int padding = 4 - 3*bmih.biWidth%4;
    BYTE *pad = new BYTE[padding]; //Массив нулей, которые будут приписаны в конце каждой строки (чтобы длина строки была кратна 4)
    for (int i = 0; i < padding; ++i)
    {
        pad[i] = 0;
    }

    //Записываем RGB-компоненты пикселей в файл
    for (int i = 0; i < bmih.biHeight; ++i)
    {
        for (int j = 0; j < bmih.biWidth; ++j)
        {
            out.write((char*)pix_colors[i][j],3);
        }
        if (3*bmih.biWidth%4 != 0) //Если строка не кратна 4, дописываем нули
        {
            out.write((char*)pad,padding);
        }
    }

    //Очищаем память
    for (int i = 0; i < bmih.biHeight; ++i)
    {
        for (int j = 0; j < bmih.biWidth; ++j)
        {
            delete[] pix_colors[i][j];
        }
        delete[] pix_colors[i];
    }
    delete[] pix_colors;

    delete[] pad;

    in.close();
    out.close();

    return 0;
}

