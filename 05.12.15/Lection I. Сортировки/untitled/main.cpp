#include <iostream>

using namespace std;


template <typename T>
bool Greater(T a, T b)
{
    return a > b;
}

template <typename T>
void Swap(T & a, T & b)
{
    T tmp = a;
    a = b;
    b = tmp;
}

template <typename T>
void printa(T *arr,int N)
{
    for (int i = 0; i < N; ++i)
        cout << arr[i] << " ";
    cout << endl << endl;
}


template <typename T>
void Heap_sort(T * arr, int N, bool (* comp)(T,T) = Greater)
{
    //Построение пирамиды
    for (int k0 = N/2-1; k0 >= 0; --k0)
    {
        int k = k0;
        while (2*k+1 < N)
        {
            if (2*k+2 < N)
            {
                int maxIdx = comp(arr[2*k+1], arr[2*k+2]) ? 2*k+1 : 2*k+2; //(if) ... ?(then) ... : (else) ...;
                if (comp(arr[maxIdx], arr[k]))
                {
                    Swap(arr[maxIdx], arr[k]);
                    k = maxIdx;
                }
                else
                    break;
            }
            else
            {
                if (comp(arr[2*k+1], arr[k]))
                {
                    Swap(arr[k], arr[2*k+1]);
                }
                break;
            }

        }
    }

    //Сортировка на базе пирамиды
    for (int i = 0; i < N; ++i)
    {
        Swap(arr[0], arr[N-1-i]);
        //Просеиваем элемент через пирамиду
        int k = 0;
        while (2*k+1 < N-i-1)
        {
            if (2*k+2 < N-i-1)
            {
                int maxIdx = comp(arr[2*k+1], arr[2*k+2]) ? 2*k+1 : 2*k+2; //(if) ... ?(then) ... : (else) ...;
                if (comp(arr[maxIdx], arr[k]))
                {
                    Swap(arr[maxIdx], arr[k]);
                    k = maxIdx;
                }
                else
                    break;
            }
            else
            {
                if (comp(arr[2*k+1], arr[k]))
                {
                    Swap(arr[k], arr[2*k+1]);
                }
                break;
            }

        }

    }
}

int main()
{    
    srand(time(NULL));
    int N = 10;
    int *arr = new int[N];
    for (int i = 0 ; i < N; ++i)
    {
        arr[i] = rand()%21-10;
    }
    printa(arr,N);

    clock_t start = clock();
    Heap_sort(arr, N);
    clock_t end = clock();

    printa(arr,N);

    cout << (double)(end-start) / CLOCKS_PER_SEC << endl;

    return 0;
}

