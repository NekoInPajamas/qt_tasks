/*
Написать программу, которая выводит все года в датах вида ‘ДД.ММ.ГГГГ’, записанные в текстовом файле, римскими числами.
*/

#include <iostream>
#include <fstream>
#include <cstring>

using namespace std;

int main()
{
    fstream fs;
    fs.open("date.txt");
    if (!fs.is_open())
    {
        cout << "Can't read the file!" << endl;
    }

    char strDate[12];

    while (!fs.eof())
    {
        fs.getline(strDate,12); //Считываем текущую строку
        if (strlen(strDate) == 10 && strDate[0] >= '0' && strDate[0] <= '3'
                && strDate[1] >= '0' && strDate[1] <= '9' && (strDate[0] - 48)*10 + (strDate[1] - 48) <= 31
                && strDate[2] == '.' && strDate[5] == '.'
                && strDate[3] >= '0' && strDate[3] <= '1'
                && strDate[4] >= '0' && strDate[4] <= '9' && (strDate[3] - 48)*10 + (strDate[4] - 48) <= 12
                && strDate[6] >= '0' && strDate[6] <= '9'
                && strDate[7] >= '0' && strDate[7] <= '9'
                && strDate[8] >= '0' && strDate[8] <= '9'
                && strDate[9] >= '0' && strDate[9] <= '9') /* Проверяем, соответствует ли строка формату даты вида ‘ДД.ММ.ГГГГ’,
                                                           (с учётом того, что месяцев -- 12, а дней -- не больше 31) */
        {
            if ( (strDate[3] == '0' && strDate[4] == '4' && (strDate[0] - 48)*10 + (strDate[1] - 48) > 30)
                 || (strDate[3] == '0' && strDate[4] == '6' && (strDate[0] - 48)*10 + (strDate[1] - 48) > 30)
                 || (strDate[3] == '0' && strDate[4] == '9' && (strDate[0] - 48)*10 + (strDate[1] - 48) > 30)
                 || (strDate[3] == '1' && strDate[4] == '1' && (strDate[0] - 48)*10 + (strDate[1] - 48) > 30)
                 || (strDate[3] == '0' && strDate[4] == '2'
                     && ((strDate[6] - 48)*1000 + (strDate[7] - 48)*100 + (strDate[8] - 48)*10 + (strDate[9] - 48)) % 4 == 0
                     && ((strDate[6] - 48)*1000 + (strDate[7] - 48)*100 + (strDate[8] - 48)*10 + (strDate[9] - 48)) % 100 != 0
                     && (strDate[0] - 48)*10 + (strDate[1] - 48) > 29)
                 || (strDate[3] == '0' && strDate[4] == '2'
                     && ((strDate[6] - 48)*1000 + (strDate[7] - 48)*100 + (strDate[8] - 48)*10 + (strDate[9] - 48)) % 400 == 0
                     && (strDate[0] - 48)*10 + (strDate[1] - 48) > 29)
                 || (strDate[3] == '0' && strDate[4] == '2'
                     && ((strDate[6] - 48)*1000 + (strDate[7] - 48)*100 + (strDate[8] - 48)*10 + (strDate[9] - 48)) % 400 != 0
                     && ((strDate[6] - 48)*1000 + (strDate[7] - 48)*100 + (strDate[8] - 48)*10 + (strDate[9] - 48)) % 100 == 0
                     && (strDate[0] - 48)*10 + (strDate[1] - 48) > 28)
                 || (strDate[3] == '0' && strDate[4] == '2'
                     && ((strDate[6] - 48)*1000 + (strDate[7] - 48)*100 + (strDate[8] - 48)*10 + (strDate[9] - 48)) % 4 != 0
                     && ((strDate[6] - 48)*1000 + (strDate[7] - 48)*100 + (strDate[8] - 48)*10 + (strDate[9] - 48)) % 100 != 0
                     && (strDate[0] - 48)*10 + (strDate[1] - 48) > 28))
            {
                cout << "This date doesn't exist!" << endl; /* Проверяем, что в апреле, июне, сентябре и ноябре дней не больше 30,
                                                            в феврале високосного года -- не более 29, в феврале невисокосного
                                                            года -- не более 28 */
            }
            else
            {
                switch (strDate[0]) //Записываем число римскими цифрами
                {
                    case '3': {cout << "XXX"; break;}
                    case '2': {cout << "XX"; break;}
                    case '1': {cout << "X"; break;}
                    case '0': {break;}
                }
                switch (strDate[1])
                {
                    case '9': {cout << "IX."; break;}
                    case '8': {cout << "VIII."; break;}
                    case '7': {cout << "VII."; break;}
                    case '6': {cout << "VI."; break;}
                    case '5': {cout << "V."; break;}
                    case '4': {cout << "IV."; break;}
                    case '3': {cout << "III."; break;}
                    case '2': {cout << "II."; break;}
                    case '1': {cout << "I."; break;}
                    case '0': {cout << "."; break;}
                }

                switch (strDate[3]) //Записываем месяц римскими цифрами
                {
                    case '1': {cout << "X"; break;}
                    case '0': {break;}
                }
                switch (strDate[4])
                {
                    case '9': {cout << "IX."; break;}
                    case '8': {cout << "VIII."; break;}
                    case '7': {cout << "VII."; break;}
                    case '6': {cout << "VI."; break;}
                    case '5': {cout << "V."; break;}
                    case '4': {cout << "IV."; break;}
                    case '3': {cout << "III."; break;}
                    case '2': {cout << "II."; break;}
                    case '1': {cout << "I."; break;}
                    case '0': {cout << "."; break;}
                }

                switch (strDate[6]) //Записываем год римскими цифрами
                {
                    case '9': {cout << "ↀ ↂ "; break;}
                    case '8': {cout << "ↁↀ ↀ ↀ "; break;}
                    case '7': {cout << "ↁↀ ↀ "; break;}
                    case '6': {cout << "ↁↀ "; break;}
                    case '5': {cout << "ↁ"; break;}
                    case '4': {cout << "ↀ ↁ "; break;}
                    case '3': {cout << "MMM"; break;}
                    case '2': {cout << "MM"; break;}
                    case '1': {cout << "M"; break;}
                    case '0': {break;}
                }
                switch (strDate[7])
                {
                    case '9': {cout << "CM"; break;}
                    case '8': {cout << "DCCC"; break;}
                    case '7': {cout << "DCC"; break;}
                    case '6': {cout << "DC"; break;}
                    case '5': {cout << "D"; break;}
                    case '4': {cout << "CD"; break;}
                    case '3': {cout << "CCC"; break;}
                    case '2': {cout << "CC"; break;}
                    case '1': {cout << "C"; break;}
                    case '0': {break;}
                }
                switch (strDate[8])
                {
                    case '9': {cout << "XC"; break;}
                    case '8': {cout << "LXXX"; break;}
                    case '7': {cout << "LXX"; break;}
                    case '6': {cout << "LX"; break;}
                    case '5': {cout << "L"; break;}
                    case '4': {cout << "XL"; break;}
                    case '3': {cout << "XXX"; break;}
                    case '2': {cout << "XX"; break;}
                    case '1': {cout << "X"; break;}
                    case '0': {break;}
                }
                switch (strDate[9])
                {
                    case '9': {cout << "IX"; break;}
                    case '8': {cout << "VIII"; break;}
                    case '7': {cout << "VII"; break;}
                    case '6': {cout << "VI"; break;}
                    case '5': {cout << "V"; break;}
                    case '4': {cout << "IV"; break;}
                    case '3': {cout << "III"; break;}
                    case '2': {cout << "II"; break;}
                    case '1': {cout << "I"; break;}
                    case '0': {break;}
                }

                cout << endl;
            }

        }
        else if (strlen(strDate) == 0) //Пустые строки пропускаем
        {
            cout << endl;
        }
        else
        {
            cout << strDate << endl; //При неправильном формате даты или при наборе текста выводим на экран строку без изменений
        }
    }

    fs.close();

    return 0;
}

