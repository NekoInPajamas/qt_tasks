#include <iostream>
#include <cstring>

using namespace std;

//Максимум из двух чисел
int max(int x, int y)
{
    if (x > y)
    {
        return x;
    }
    else
    {
        return y;
    }
}

//Максимальный элемент массива
int max(int *arr, int N)
{
    int tmp = arr[0];

    for (int i = 1; i < N; ++i)
    {
        if (arr[i] > tmp)
        {
            tmp = arr[i];
        }
    }
    return tmp;
}

//Структура точка с полями -- координатами этой точки
struct Point
{
    int x;
    int y;

    Point(int x1 = 0, int y1 = 0) //Конструктор объекта при инициализации
    {
        x = x1;
        y = y1;
    }
};

//Точка с максимальным x
bool more(Point p1, Point p2)
{
    return (p1.x > p2.x);
}

//Точка с максимальной суммой её координат x и y
bool more_sum(Point p1, Point p2)
{
    return (p1.x + p1.y > p2.x + p2.y);
}

bool more_int(int x, int y)
{
    return x > y;
}

//Point max(Point *arr, int N, bool (*f)(Point,Point))
//{
//    Point tmp = arr[0];

//    for (int i = 1; i < N; ++i)
//    {
//        if (f(arr[i],tmp) == true)
//        {
//            tmp = arr[i];
//        }
//    }
//    return tmp;
//}

//int max(int *arr, int N, bool (*f)(int,int))
//{
//    int tmp = arr[0];

//    for (int i = 1; i < N; ++i)
//    {
//        if (f(arr[i],tmp) == true)
//        {
//            tmp = arr[i];
//        }
//    }
//    return tmp;
//}

//Шаблоны
template <typename T>
T max(T* arr, int N, bool (*f)(T,T))
{
    T tmp = arr[0];

    for (int i = 1; i < N; ++i)
    {
        if (f(arr[i],tmp) == true)
        {
            tmp = arr[i];
        }
    }
    return tmp;
}

bool more_str(char * str1, char * str2)
{
    return (strlen(str1) > strlen(str2));
}

void myprintf()
{
    cout << "No parameters" << endl;
}

//Переменное количество параметров любого типа
template <typename T, typename ... Args>
void myprintf(T val, Args ... args)
{
    cout << val << endl;
    myprintf(args...); //Рекурсивный вызов (следующая функция будет без первого параметра текущей функции)
}

int main()
{
    //Максимум из двух чисел
    cout << max(4,5) << endl;

    //Максимальный элемент массива
    int arr[3] = {1,2,3};
    cout << max(arr,3) << endl;

    Point arr_p[3] = {Point(5,6), Point(2,3), Point(10,0)};

    //Точка с максимальным x
    Point p_max = max(arr_p,3,more);
    cout << "P max x: " << p_max.x << ", P max y: " << p_max.y << endl;

    //Точка с максимальной суммой её координат x и y
    Point p_max_sum = max(arr_p,3,more_sum);
    cout << "P max_sum x: " << p_max_sum.x << ", P max_sum y: " << p_max_sum.y << endl;

    //Строка с максимальной длиной
    char * arr_str[3] = {"str","strstr","str1"};
    cout << "Max string: " << max(arr_str,3,more_str) << endl;


    myprintf(1,"str",true,2.5,'c',max(arr_str,3,more_str));

    return 0;
}

