/*
Задача вычисления определённого интеграла методом трапеций
*/

#include <iostream>
#include <cmath>
#include <cstring>

using namespace std;

int sum(int x, int y)
{
    return x+y;
}

double func(double x)
{
    return 2*sin(x)+cos(x);
}

double func1(double x)
{
    return x*x + 2*x;
}

double integral(double (*f)(double), double a, double b, int N)
{
    double h = (b - a)/N;
    double a1 = a;
    double b1 = a + h;
    double sum = 0;

    for (int i = 0; i < N; ++i)
    {
        sum += 0.5*h*(f(a1) + f(b1));
        a1 = b1;
        b1 += h;
    }

    return sum;
}

int main()
{
    int (*sumP)(int, int);
    sumP = sum;

    cout << sum(1,2) << endl;
    cout << sumP(3,4) << endl;

    cout << "Integral1: " << integral(func, 0, 1, 1000) << endl;
    cout << "Integral2: " << integral(func1, 0, 1, 1000) << endl;

    return 0;
}

