#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int x, y, z;
    double res;

    cout << "Enter x, y, z" << endl;
    cin >> x >> y >> z;

    if (z==0)
    {
        cout << "Error, z=0!" << endl;
        return 1;
    }
    else
    {
          res = sqrt(x*x+y*y)/z;
          cout << "Result is: " << res << endl;
    }

    return 0;
}
