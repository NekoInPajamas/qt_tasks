#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int i = 11;
    int x = 5;
    double d = 25.0;

    cout << pow(d,2) << endl;

    if (x>0)
        cout << "Greater" << endl;
    else
        cout << "Less" << endl;

/*Краткая форма if:
  x > 0 ? cout << "Greater" : cout << "Greater";
*/

    int num1, num2;
    double res;
    char c;
    cout << "Enter number 1 and number 2" << endl;
    cin >> num1 >> num2;
    cout << "Enter operation" << endl;
    cin >> c;

    switch (c) {
    case '+':
        res = num1+num2; break;
    case '-':
        res = num1-num2; break;
    case '*':
        res = num1*num2; break;
    case '/':
        res = num1/num2; break;
    case '%':
        res = num1%num2; break;
    default:
        cout << "Smth wrong" << endl;
        return 1;
    }
    cout << "The result of operation is " << res << endl;

    for (int count=0; count<5; ++count)
    {
        cout << "Count is: " << count << endl;
    }

    return 0;
}
