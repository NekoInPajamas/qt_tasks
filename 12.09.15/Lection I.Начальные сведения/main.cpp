//Подключение библиотеки для вывода в консоль
#include <iostream>

//Выбор пространства имён
using namespace std;

#define pi 3.14

//Главная функция
int main()
{
    //Инициализация переменных
    int x =10;
    double l = pi;

    //Явное преобразование типов
    double y = double(x);
    int a = 1, b = 2, c = 3;

    //Инициализация в разных системах счисления
    int oct = 010; //8 в десятичной
    int hex = 0x10; //16 в десятичной
    cout << "oct: " << oct << endl;

    //Инициализация беззнаковой переменной
    unsigned int uns = -15;
    cout << uns << endl;
    const int p = 100;

    //Вывод строки на консоль
    //endl -- переход на новую строку
    cout<<"priueet!"<<endl;
    cout << "x is " << x << endl;
    cout << "y is " << y << endl;

    int z = 0, v = 0;
    cout << "Type z and v: " << endl;
    cin >> z >> v;
    cout << "z and v: " << z << " " << v << endl;

    cout << "Size of int: " << sizeof(x) << endl;

    //system("pause"); -- ожидание нажатия клавиши
    //Возвращение значения функции
    return 0;
}

