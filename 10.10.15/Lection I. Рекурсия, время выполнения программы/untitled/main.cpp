#include <iostream>
#include <ctime>

using namespace std;

//Рекурсивное вычисление n-го члена последовательности Фибоначчи
int fib_rec(int n)
{
    if (n == 0 || n == 1)
    {
        return 1;
    }
    return fib_rec(n-1) + fib_rec(n-2);
}

int main()
{
    int n;
    cout << "Введите n" << endl;
    cin >> n;

    clock_t start = clock();

    int fib = 0;
    int fib_prev = 0;
    int fib_prev_prev = 1;

    for (int i = 0; i < n; ++i)
    {
        fib = fib_prev + fib_prev_prev;
        fib_prev_prev = fib_prev;
        fib_prev = fib;
        cout << fib << " ";
    }

    cout << endl;

    clock_t end = clock();

    double t1 = (double)(end - start)/CLOCKS_PER_SEC;

    clock_t beginT = clock();

    for (int i = 0; i < n;++i)
    {
        cout << "Fibonacci recursive: " << fib_rec(i) << endl;
    }

    clock_t stopT = clock();
    double t2 = (double)(stopT - beginT)/CLOCKS_PER_SEC;
    cout << "t1 = " << t1 << endl;
    cout << "t2 = " << t2 << endl;

    return 0;
}

