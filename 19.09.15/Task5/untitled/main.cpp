/*
Найти решение системы уравнений.
*/

#include <iostream>

using namespace std;

int main()
{
    double a1, a2, b1, b2, c1, c2;
    double det, det1, det2;
    double x, y;

    cout << "Введите коэффициенты первого уравнения" << endl;
    cin >> a1 >> b1 >> c1;
    cout << "Введите коэффициенты второго уравнения" << endl;
    cin >> a2 >> b2 >> c2;

    //Вычисляем определители (det1 и det2 используются в числителе в правиле Крамера)
    det = (a1*b2 - a2*b1);
    det1 = (-c1*b2 - a2*(-c2));
    det2 = (a1*(-c2) - (-c1)*b1);

    //Проверка определителей
    if (det == 0 && det1 == 0 && det2 == 0)
    {
        cout << "Система имеет бесконечно много решений" << endl;
    }
    else if (det == 0 && (det1 != 0 || det2 != 0))
    {
        cout << "Система не имеет решений" << endl;
    }
    else
    {
    x = det1/det;
    y = det2/det;

    cout << "Решение системы: x = " << x << ", y = " << y << endl;
    }
    return 0;
}

