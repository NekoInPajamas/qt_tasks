/*
Написать программу для расчета по двум формулам.
*/

#include <iostream>
#include <cmath>

using namespace std;

#define pi 3.14

int main()
{
    //Ввод угла (в радианах)
    double alpha_rad;
    cin >> alpha_rad;

    //double alpha = alpha_rad*(180/pi);

    //Подсчёт по формулам и проверка знаменателя
    double z1, z2;

    if (1 + sin(2*alpha_rad) == 0 || 1 + tan(alpha_rad) == 0)
    {
        cout << "Error, denominator is equal to zero!" << endl;
        return 1;
    }
    else
    {
    z1 = (1 - 2*sin(alpha_rad)*sin(alpha_rad))/(1 + sin(2*alpha_rad));
    z2 = (1 - tan(alpha_rad))/(1 + tan(alpha_rad));
    }

    //Вывод ответа
    cout << "z1 = " << z1 << endl;
    cout << "z2 = " << z2 << endl;

    return 0;
}

