#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    //Источник рандомизации
    srand(time(NULL));
    //int r = rand()%10; //от 0 до 9
    int r[3] = {0};
    int min, max;
    for (int i = 0; i < 3; i++)
    {
        r[i] = rand()%100;
        cout << r[i] << "\t";
    }
    min = max = r[0];
    for (int i = 0; i < 3; i++)
    {
        if (r[i] < min) min = r[i];
        if (r[i] > max) max = r[i];
    }
    cout << endl;
    cout << "MIN: " << min << endl;
    cout << "MAX: " << max << endl;

    char str_n[] = {'H', 'e', 'l', 'l', 'o'};
    cout << str_n << " " << sizeof(str_n) << endl;
    char str[] = "Hello";
    cout << str << " " << sizeof(str) << endl;

    char c = 'A';
    cout << (int)c << endl;

    return 0;
}
