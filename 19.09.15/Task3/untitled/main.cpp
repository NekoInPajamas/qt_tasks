/*
Поменять значения двух целочисленных переменных одного типа, не используя
дополнительной переменной.
*/

#include <iostream>
using namespace std;

int main()
{
    //Ввод двух чисел
    int a, b;
    cout << "Enter a" << endl;
    cin >> a;
    cout << "Enter b" << endl;
    cin >> b;

    //Переменные меняют своё значение
    a = a + b;
    b = a - b;
    a = a - b;

    //Вывод результата
    cout << "a = " << a << ", b = " << b << endl;

    return 0;
}

