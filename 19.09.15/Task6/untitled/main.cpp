/*
Написать программу для решения квадратного уравнения.
*/

#include <iostream>
#include<cmath>

using namespace std;

int main()
{
    double a, b, c;
    double discrmnt;
    double x1, x2, x, xRe, xIm;

    //Ввод коэффициентов уравнения
    cout << "Введите коэффициенты квадратного уравнения" << endl;
    cin >> a >> b >> c;

    //Подсчёт дискриминанта
    discrmnt = b*b - 4*a*c;

    //Нахождение решения
    if (discrmnt > 0)
    {
        x1 = (-b + sqrt(discrmnt))/(2*a);
        x2 = (-b - sqrt(discrmnt))/(2*a);
        cout << "Решение квадратного уравнения: x1 = " << x1 << ", x2 = " << x2 << endl;
    }
    else if (discrmnt == 0)
    {
        x = -b/(2*a);
        cout << "Решение квадратного уравнения: x = " << x << endl;
    }
    else
    {
        xRe = -b/(2*a);
        xIm = sqrt(abs(discrmnt))/(2*a);

        cout << "Решение квадратного уравнения: x1 = " << xRe << "+" << xIm << "i, x2 = " << xRe << "-" << xIm << "i" << endl;
    }
    return 0;
}

