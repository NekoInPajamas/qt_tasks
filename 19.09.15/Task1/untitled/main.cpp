/*
Заданы два момента времени в часах, минутах и секундах (в пределах одних
суток). Найти продолжительность промежутка времени между ними в тех же единицах.
*/

#include <iostream>

using namespace std;

int main()
{
    //Вводится первый момент времени
    int mom_1[3], mom_2[3];
    cout << "Enter first moment" << endl;
    for (int i = 0; i < 3; i++)
    {
        cin >> mom_1[i];
        //cout << mom_1[i] << endl;
    }

    //Проверка на то, что время введено корректно
    if (mom_1[0] > 24 || mom_1[1] > 59 || mom_1[2] > 59)
    {
        cout << "Error, wrong time format" << endl;
        return 1;
    }

    //Вводится второй момент времени
    cout << "Enter second moment" << endl;
    for (int i = 0; i < 3; i++)
    {
        cin >> mom_2[i];
        //cout << mom_2[i] << endl;
    }

    //Проверка на то, что время введено корректно
    if (mom_2[0] > 24 || mom_2[1] > 59 || mom_2[2] > 59)
    {
        cout << "Error, wrong time format" << endl;
        return 1;
    }

    //Перевод времени в секунды
    int time_1 = mom_1[0]*3600 + mom_1[1]*60 + mom_1[2];
    int time_2 = mom_2[0]*3600 + mom_2[1]*60 + mom_2[2];

    //Подсчёт разницы
    int dif;
    if (time_1 < time_2)
        dif = time_2 - time_1;
    else
        dif = time_1 - time_2;

    //Перевод разницы в удобоваримый формат
    int hour = dif/3600;
    int min = (dif - hour*3600)/60;
    int sec = dif - hour*3600 - min*60;

    //Вывод разницы и приведение к привычному виду
    cout << "Difference is ";
    if (hour < 10)
        cout << "0" << hour << ":";
    else cout << hour << ":";

    if (min < 10)
        cout << "0" << min << ":";
    else cout << min << ":";

    if (sec < 10)
        cout << "0" << sec << endl;
    else cout << sec << endl;

    return 0;
}

