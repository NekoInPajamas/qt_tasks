/*
Даны координаты трех вершин треугольника. Найти его периметр и площадь.
*/

#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    double x1, y1, x2, y2, x3, y3;
    double side1, side2, side3;
    double perimeter, area;

    //Ввод координат вершин треугольника
    cout << "Введите координаты первой вершины" << endl;
    cin >> x1 >> y1;
    cout << "Введите координаты второй вершины" << endl;
    cin >> x2 >> y2;
    cout << "Введите координаты третьей вершины" << endl;
    cin >> x3 >> y3;

    //Подсчёт длин сторон треугольника
    side1 = sqrt(pow((x1 - x2), 2) + pow((y1 - y2), 2));
    side2 = sqrt((x2 - x3)*(x2 - x3) + (y2 - y3)*(y2 - y3));
    side3 = sqrt((x3 - x1)*(x3 - x1) + (y3 - y1)*(y3 - y1));

    //Проверка на то, что является ли треугольник вырожденным
    if (side3 >= side1 + side2 || side2 >= side1 + side3 || side1 >= side2 + side3)
    {
        cout << "Треугольник является вырожденным!" << endl;
        return 1;
    }
    else
    {
    //Подсчёт периметра и площади треугольника
    perimeter = side1 + side2 + side3;
    area = sqrt(0.5*perimeter * (0.5*perimeter - side1) * (0.5*perimeter - side2) * (0.5*perimeter - side3));

    //Вывод результата
    cout << "Периметр треугольника равен " << perimeter << endl;
    cout << "Площадь треугольника равна " << area << endl;
    }

    return 0;
}

