#include <iostream>

using namespace std;

template <typename T>
bool Greater(T a, T b)
{
    return a > b;
}

template <typename T>
void Swap(T & a, T & b)
{
    T tmp = a;
    a = b;
    b = tmp;
}

template <typename T>
void printa(T *arr,int N)
{
    for (int i = 0; i < N; ++i)
        cout << arr[i] << " ";
    cout << endl << endl;
}


//Пузырьковая сортировка
template <typename T>
void bubble_sort(T *arr, int N, bool (*comp)(T,T) = Greater)
{
    for (int i = 1; i < N; ++i)
    {
        for (int j = 0; j < N-i; ++j)
        {
            if (comp(arr[j],arr[j+1]) == true)
            {
                Swap(arr[j],arr[j+1]);
            }
        }
    }
}

//Сортировка выбором
template <typename T>
void selection_sort(T *arr, int N,bool (*comp)(T,T) = Greater)
{
    for (int i = 0; i < N-1; ++i)
    {
        int idx = 0;
        for (int j = 1; j < N-i; ++j)
        {
            if (comp(arr[j],arr[idx]) == true)
                idx = j;
        }

        Swap(arr[N-i-1],arr[idx]);
    }
}

//Сортировка вставками
template <typename T>
void insertion_sort(T *arr, int N, bool (*comp)(T,T) = Greater)
{
    for (int i = 0; i < N-1; ++i)
    {
        T key = arr[i+1];
        int j = i;
        for (; j >= 0; --j)
        {
            if (comp(arr[j], key) == true)
            {
                arr[j+1] = arr[j];
            }
            else
                break;
        }
        if (j != i)
        {
            arr[j+1] = key;
        }
    }
}

//Сортировка Шелла
template <typename T>
void Shell_sort(T * arr, int N, bool (*comp)(T, T) = Greater)
{
    int s = 1;
    for (; s < N/3; s = 3*s+1);

    for (int h = s; h >= 1; h/=3)
    {
        for (int i = h; i < 2*h; ++i)
        {
            for (int j = i-h; j < N-h; j += h)
            {
                T key = arr[j+h];
                int k = j;
                for (; k >= 0; k -= h)
                {
                    if (comp(arr[k], key) == true)
                    {
                        arr[k+h] = arr[k];
                    }
                    else
                        break;
                }
                if (k != j)
                {
                    arr[k+h] = key;
                }
            }
        }
    }
}

int main()
{
    srand(time(NULL));
        int N = 10000;
        int *arr = new int[N];
        for (int i = 0 ; i < N; ++i)
        {
            arr[i] = rand()%21-10;
        }
        printa(arr,N);

        clock_t start = clock();
        //bubble_sort(arr,N);
        //selection_sort(arr,N);
        //insertion_sort(arr, N);
        Shell_sort(arr, N);
        clock_t end = clock();

        printa(arr,N);

        cout << (double)(end-start) / CLOCKS_PER_SEC << endl;

    return 0;
}

