#ifndef FUNCTIONS
#define FUNCTIONS

double func1(double);
double func2(double);
double func3(double);
double func4(double);
void laplace(double, double, int, int, double &, double &, double **, double,
             double(*)(double), double(*)(double), double(*)(double), double(*)(double));

#endif // FUNCTIONS

