#include <iostream>
#include <cmath>
#include "functions.h"

using namespace std;

int main()
{
    cout << fixed;
    cout.precision(3);
    cout.fill('0');
    int m = 21, n = 21; //Количество узлов сетки
    double a = 1, b = 1; //Граница сетки
    double eps = 0.0001;

    double stepX = 0, stepY = 0;

    double ** u = new double * [m];
    for (int i = 0; i < m; ++i)
    {
        u[i] = new double [n];
    }

    laplace(a, b, m, n, stepX, stepY, u, eps, func1, func2, func3, func4);

    //Вывод решения
    cout << "x\\y\t ";
    for (int j = 0; j < n; ++j)
    {
        cout <<" "<< stepY*j << " ";
    }
    cout << endl << endl;

    for (int i = 0; i < m; ++i)
    {
        cout << stepX*i << "\t ";
        for (int j = 0; j < n; ++j)
        {
            if (u[i][j]>=0)
                cout << " ";
//            if (abs(u[i][j])<10)
//                cout << " ";
            cout << u[i][j] << " ";
        }
        cout << endl;
    }

    //Очистка памяти
    for (int i = 0; i < m; ++i)
    {
        delete[] u[i];
    }
    delete[] u;

    return 0;
}

