#include <cmath>
#include "functions.h"

using namespace std;

//Граничные условия
double func1(double y)
{
    return -6*y*y - 4*y + 2;
}
double func2(double y)
{
    return -6*y*y - 18*y + 10;
}
double func3(double x)
{
    return 5*x*x + 3*x + 2;
}
double func4(double x)
{
    return 5*x*x - 11*x - 8;
}

//Решение задачи Дирихле для уравнения Лапласа
void laplace(double a, double b, int m, int n, double & stepX, double & stepY, double ** u, double eps,
             double(*f1)(double), double(*f2)(double), double(*f3)(double), double(*f4)(double))
{
    stepX = a/(m-1); //Шаг по X
    stepY = b/(n-1); //Шаг по Y
    double coef = (stepX/stepY)*(stepX/stepY);

    //Числитель и знаменатель из правой части условия окончания итерационного процесса
    double difCurrent = 10e+10; //Числитель
    double difPrevious = 0; //Знаменатель

    //Вычисление решения на границах
    double xi = 0, yj = 0;

    for (int j = 0; j < n; ++j)
    {
        yj = stepY*j;
        u[0][j] = f1(yj);
        u[m-1][j] = f2(yj);
    }

    for (int i = 0; i < m; ++i)
    {
        xi = stepX*i;
        u[i][0] = f3(xi);
        u[i][n-1] = f4(xi);
    }

    //Нулевое приближение на узлах сетки
    for (int i = 1; i < m-1; ++i)
    {
        for (int j = 1; j < n-1; ++j)
        {
            u[i][j] = 1;
        }
    }

    //Решение линейной системы методом Гаусса-Зейделя
    double uCurrent = 0;
    double difEps = eps;
    double dif = 0;

    do
    {
        difPrevious = difCurrent;
        difCurrent = 0;
        for (int i = 1; i < m-1; ++i)
        {
            for (int j = 1; j < n-1; ++j)
            {
                uCurrent = 0.5*(u[i+1][j] + u[i-1][j] + coef*u[i][j+1] + coef*u[i][j-1])/(1 + coef);
                dif = abs(uCurrent - u[i][j]);
                if (dif > difCurrent)
                {
                    difCurrent = dif;
                }
                u[i][j] = uCurrent;
            }
        }
        difEps = eps*(1 - difCurrent/difPrevious);
    }
    while (difCurrent >= difEps);
}

