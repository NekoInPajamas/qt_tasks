#include <cmath>
#include "functions.h"

double func1(double Xi)
{
     switch ((int) (Xi*10))
     {
        case 0: {return -1.7930; break;}
        case 2: {return -1.7863; break;}
        case 4: {return -1.7832; break;}
        case 6: {return -1.7838; break;}
        case 8: {return -1.7878; break;}
        case 10: {return -1.7953; break;}
     }
}

double func2(double Xi)
{
    double a = 0.71;
    return cos(a*Xi);
}

double func3(double Xi)
{
    return 2*Xi*Xi + 2*Xi - 4;
}
