#include <iostream>
#include <cmath>
#include "functions.h"

using namespace std;

int main()
{
    int n = 6; //Количество узлов сетки
    double x0 = 0; //Нижняя граница сетки
    double xN = 1; //Верхняя граница сетки

    double stepX = (xN - x0)/(n - 1); //Шаг по x

    double * x = new double [n]; //Матрица для хранения узлов сетки
    for (int i = 0; i < n; ++i)
    {
        x[i] = x0 + stepX*i;        
    }    

    double * y = new double [n]; //Массив, содержащий значения y

    //Краевые условия
    double alpha0 = 1, alpha1 = 0, A = 0;
    double beta0 = 1, beta1 = 0, B = 0;

    //Найдём коэффициенты уравнения, полученного заменой исходного ДУ центральными конечно-разностными отношениями
    double * m = new double [n];    
    for (int i = 1; i < n; ++i)
    {
        m[i] = (2*func2(x[i])*stepX*stepX - 4) / (2 + stepX*func1(x[i]));
    }

    double * k = new double [n];    
    for (int i = 1; i < n; ++i)
    {
        k[i] = (2 - stepX*func1(x[i])) / (2 + stepX*func1(x[i]));
    }

    double * phi = new double [n];    
    for (int i = 1; i < n; ++i)
    {
        phi[i] = (2*stepX*stepX*func3(x[i])) / (2 + stepX*func1(x[i]));
    }    

    //Найдём коэффициенты уравнения Y[i] = C[i]*(D[i] - Y[i+1])
    double * c = new double [n];
    c[1] = (alpha1 - alpha0*stepX) / (m[1]*(alpha1 - alpha0*stepX) + k[1]*alpha1);
    for (int i = 2; i < n; ++i)
    {
        c[i] = 1 / (m[i] - k[i]*c[i-1]);
    }

    double * d = new double [n];
    d[1] = phi[1] - (k[1]*A*stepX) / (alpha1 - alpha0*stepX);
    for (int i = 2; i < n; ++i)
    {
        d[i] = phi[i] - k[i]*c[i-1]*d[i-1];
    }

    //Обратный ход: ищем решения уравнения
    y[n-1] = (2*B*stepX - beta1*(d[n-1] - c[n-2]*d[n-2])) / (2*beta0*stepX + beta1*(c[n-2] - (1/c[n-1])));
    for (int i = n-2; i > 0; --i)
    {
        y[i] = c[i]*(d[i] - y[i+1]);
    }
    y[0] = (alpha1*y[1] - A*stepX) / (alpha1 - alpha0*stepX);

    cout << "x[i]  m[i]  \t  k[i] \t phi[i]\t   c[i]  \t  d[i]  \t  y[i]" << endl;
    cout << x[0] << "\t\t\t\t\t\t\t\t" << y[0] << endl;
    for (int i = 1; i < n-1; ++i)
    {
        cout << x[i] << "  " << m[i] << "   " << k[i] << " " << phi[i] << " " << c[i] << "   " << d[i] << "\t\t" << y[i] << endl;
    }
    cout << x[n-1] << "    " << m[n-1] << "   " << k[n-1] << "  " << phi[n-1] << "\t" << c[n-1] << "   " << d[n-1] << "\t\t" << y[n-1] << endl;

    //Очистка памяти
    delete[] x;
    delete[] y;
    delete[] m;
    delete[] k;
    delete[] phi;
    delete[] c;
    delete[] d;

    return 0;
}

