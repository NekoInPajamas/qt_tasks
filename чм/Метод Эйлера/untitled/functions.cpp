#include <cmath>
#include "functions.h"

//Правая часть первого уравнения системы
double func1(double Xi, double Y1i, double Y2i)
{
    return atan(1/(1+Y1i*Y1i) + Y2i*Y2i);
}

//Правая часть второго уравнения системы
double func2(double Xi, double Y1i, double Y2i)
{
    return sin(Y1i*Y2i);
}

//Функции для вычисления приближённых значений решения системы в узлах X[i]
double * eyler(double Xi, double Y1i, double Y2i, double stepX, double (* f1)(double, double, double),
                   double (* f2)(double, double, double))
{
    double Y[2];
    Y[0] = Y1i + stepX*f1(Xi, Y1i, Y2i);
    Y[1] = Y2i + stepX*f2(Xi, Y1i, Y2i);

    return Y;
}
