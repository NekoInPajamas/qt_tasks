#include <iostream>
#include <cmath>
#include "functions.h"

using namespace std;

int main()
{
    int n = 41; //Количество узлов сетки
    double x0 = 1; //Нижняя граница сетки
    double xN = 3; //Верхняя граница сетки

    double stepX = (xN - x0)/(n - 1); //Шаг по x  

    double * x = new double [n]; //Матрица для хранения узлов сетки

    for (int i = 0; i < n; ++i)
    {
        x[i] = x0 + stepX*i;
    }

    double * y1 = new double [n]; //Массив, содержащий значения y1
    double * y2 = new double [n]; //Массив, содержащий значения y2

    y1[0] = 1;
    y2[0] = 1;

    double * Y;

    cout << "x = " << x[0] << "  y(1) = " << y1[0] << "  y(2) = " << y2[0] << endl;

    for (int i = 0; i < n-1; ++i) //Вычисляем приближённые значения решения системы в узлах x[i]
        {
            Y = rungeKutta(x[i], y1[i], y2[i], stepX, func1, func2);
            y1[i+1] = Y[0]; //*Y
            y2[i+1] = Y[1]; //*(Y+1)

            cout << "x = " << x[i+1] << "  y(1) = " << y1[i+1] << "  y(2) = " << y2[i+1] << endl;
        }

        //Очистка памяти
        delete[] x;
        delete[] y1;
        delete[] y2;

    return 0;
}

