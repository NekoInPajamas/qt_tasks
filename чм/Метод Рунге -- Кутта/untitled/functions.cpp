#include <cmath>
#include "functions.h"

//Правая часть первого уравнения системы
double func1(double Xi, double Y1i, double Y2i)
{
    return (Y1i*Y1i)/Y2i;
}

//Правая часть второго уравнения системы
double func2(double Xi, double Y1i, double Y2i)
{
    return Y1i*Y1i - Y2i;
}

//Функции для вычисления приближённых значений решения системы в узлах X[i]
double * rungeKutta(double Xi, double Y1i, double Y2i, double stepX, double (* f1)(double, double, double),
                   double (* f2)(double, double, double))
{
    double K1_1 = f1(Xi, Y1i, Y2i);
    double K2_1 = f2(Xi, Y1i, Y2i);
    double K1_2 = f1(Xi + stepX/2, Y1i + stepX*K1_1/2, Y2i + stepX*K2_1/2);
    double K2_2 = f2(Xi + stepX/2, Y1i + stepX*K1_1/2, Y2i + stepX*K2_1/2);
    double K1_3 = f1(Xi + stepX/2, Y1i + stepX*K1_2/2, Y2i + stepX*K2_2/2);
    double K2_3 = f2(Xi + stepX/2, Y1i + stepX*K1_2/2, Y2i + stepX*K2_2/2);
    double K1_4 = f1(Xi + stepX, Y1i + stepX*K1_3, Y2i + stepX*K2_3);
    double K2_4 = f2(Xi + stepX, Y1i + stepX*K1_3, Y2i + stepX*K2_3);

    double Y[2];
    Y[0] = Y1i + (stepX/6)*(K1_1 + 2*K1_2 + 2*K1_3 + K1_4);
    Y[1] = Y2i + (stepX/6)*(K2_1 + 2*K2_2 + 2*K2_3 + K2_4);

    return Y;
}
