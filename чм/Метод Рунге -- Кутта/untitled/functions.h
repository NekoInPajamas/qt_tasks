#ifndef FUNCTIONS
#define FUNCTIONS

double func1(double, double, double);
double func2(double, double, double);
double * rungeKutta(double, double, double, double, double (*)(double, double, double),
                   double (*)(double, double, double));

#endif // FUNCTIONS

