#ifndef FUNCTIONS
#define FUNCTIONS

double func(double);
double mu1(double);
double mu2(double);
void heatEq(double, double, int, int, double &, double &, double **,
            double(*)(double), double(*)(double), double(*)(double));

#endif // FUNCTIONS

