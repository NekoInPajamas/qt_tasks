#include <cmath>
#include "functions.h"

using namespace std;

//Граничные условия
double func(double x)
{
    return 1/(1+x*x);
}
double mu1(double t)
{
    return 1;
}
double mu2(double t)
{
    return 0.5;
}

//Решение смешанной задачи для однородного уравнения теплопроводности
void heatEq(double xMax, double timeMax, int m, int n, double & stepX, double & stepT,
            double ** u, double(*f)(double), double(*mju1)(double), double(*mju2)(double))
{
    stepX = xMax/(m-1); //Шаг по X
    stepT = timeMax/(n-1); //Шаг по t

    //Вычисление решения на границах
    double xi = 0, tj = 0;

    for (int i = 0; i < m; ++i)
    {
        xi = stepX*i;
        u[i][0] = f(xi);
    }

    for (int j = 0; j < n; ++j)
    {
        tj = stepT*j;
        u[0][j] = mju1(tj);
        u[m-1][j] = mju2(tj);
    }

    //Решение линейной системы методом прогонки: lambda*u[i-1][j] - (1 + 2*lambda)*u[i][j] + lambda*u[i+1][j] = -u[i][j-1]
    double lambda = stepT/(stepX*stepX);
    double coef = 1 + 2*lambda;

    double * alpha = new double [m]; //Вспомогательные прогоночные коэффициенты
    double * beta = new double [m];

    for (int j = 0; j < n-1; ++j)
    {
        alpha[0] = 0;
        beta[0] = u[0][j];

        for (int i = 1; i < m-1; ++i) //Вычисление прогоночных коэффициентов
        {
            alpha[i] = lambda/(coef - lambda*alpha[i-1]);
            beta[i] = (u[i][j] + lambda*beta[i-1])/(coef - lambda*alpha[i-1]);
        }

        for (int i = m-2; i > 0; --i) //Решение системы
        {
            u[i][j+1] = alpha[i]*u[i+1][j] + beta[i];
        }
    }

delete[] alpha;
delete[] beta;
}
