#include <iostream>
#include <cmath>
#include "functions.h"

using namespace std;

int main()
{
    cout << fixed;
    cout.precision(3);
    cout.fill('0');

    int m = 21, n = 41; //Количество узлов сетки
    double xMax = 1; //Граница сетки
    double timeMax = 2.0; //Ограничение по времени

    double stepX = 0, stepT = 0;

    double ** u = new double * [m];
    for (int i = 0; i < m; ++i)
    {
        u[i] = new double [n];
    }

    heatEq(xMax, timeMax, m, n, stepX, stepT, u, func, mu1, mu2);

    //Вывод решения
    cout << "t\\x\t";
    for (int i = 0; i < m; ++i)
    {
        cout <<" "<< stepX*i << " ";
    }
    cout << endl << endl;

    for (int j = 0; j < n; ++j)
    {
        cout << stepT*j << "\t";
        for (int i = 0; i < m; ++i)
        {
            if (u[i][j] >= 0)
                cout<<" ";
            cout << u[i][j] << " ";
        }
        cout << endl;
    }

    //Очистка памяти
    for (int i = 0; i < m; ++i)
    {
        delete[] u[i];
    }
    delete[] u;    

    return 0;
}

