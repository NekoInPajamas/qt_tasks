/*
Написать программу, которая проверяет, является ли введённая строка двоичным числом
*/

#include <iostream>
#include <ctime>
#include <cstring>

using namespace std;

//Функция, проверяющая, является ли введённая строка двоичным числом
bool IsBinary(char * s)
{
    int n = strlen(s);

    for (int i = 0; i < n; ++i)
    {
        if (s[i] != '0' && s[i] != '1')
        {
            return false;
        }
    }
    return true;
}

int main()
{
//    srand(time(NULL));
    char s[32];

//    for (int i = 0; i < 10; ++i) //С 48 по 122 символ
//    {
//        s[i] = rand()%75+48;
//    }

    cin >> s;

    cout << IsBinary(s) << endl;

    return 0;
}

