/*
Написать программу, которая преобразует введенное пользователем десятичное число в число в указанной системе счисления (от 2 до 10).
*/

#include <iostream>
#include <cstring>
#include <cmath>

using namespace std;

//Функция, переводящая строку в число
int strToDec(char * s)
{
    int n = strlen(s);

    int num = 0;
    for (int i = 0; i < n; ++i)
    {
        num += ((int)(s[i] - 48))*pow(10,n-1-i); //Переводим строку в число ("0" в таблице символ имеет номер "48")
    }

    return num;
}

//Функция, переводящая десятичное число в введённую систему счисления
int decToSomeSystem(char * s, int someSyst, int (*f)(char *))
{
    int num = f(s);
    int numInNewSystem;
    if (num >= someSyst)
    {
        numInNewSystem = num%someSyst;
        int k = 1;

        do
        {
            num /= someSyst; //Последовательно делим исходное число и получаемые частные на основание новой системы счисления,
            numInNewSystem += (num%someSyst)*pow(10,k);
            ++k;
        }
        while (num/someSyst >= someSyst); //пока не получится частное, меньшее основания новой системы счисления

        numInNewSystem += (num/someSyst)*pow(10,k); //Последнее частное -- старшая цифра числа в новой системе счисления,
                                                    //а последующие цифры -- это остатки от деления исходного числа
                                                    //и получаемых частных на основание новой системы счисления, записываемые
                                                    //в порядке,обратном их получению
    }
    else
    {
        numInNewSystem = num;
    }

    return numInNewSystem;
}

int main()
{
    char s[32];    
    cout << "Введите число" << endl;
    cin >> s;

    int syst;
    cout << "Введите желаемую систему счисления" << endl;
    cin >> syst;

    //cout << strToDec(s) << endl;
    cout << decToSomeSystem(s, syst, strToDec) << endl;

    return 0;
}

