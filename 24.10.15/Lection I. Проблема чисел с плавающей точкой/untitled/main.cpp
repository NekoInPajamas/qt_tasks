#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    //Погрешности перевода чисел с плавающей точкой в двоичную систему и обратно (можно обойти переводом в int)
        cout << endl;
        cout << "Погрешности перевода чисел с плавающей точкой в двоичную систему и обратно:" << endl;
        double x = 0.1;
        //int y;
        cout.precision(32);
        while (x < 2)
        {
            cout << x << " " << abs(x-1) << " " << (abs(x-1) >= 0.1) << endl;
            x += 0.1;
            //y = x;
        }

    return 0;
}

