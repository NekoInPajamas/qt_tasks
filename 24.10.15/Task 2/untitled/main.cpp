/*
Написать программу, которая переводит введённое с клавиатуры двоичное число в десятичное.
*/

#include <iostream>
#include <cstring>

using namespace std;

//Функция, проверяющая, является ли введённая строка двоичным числом
bool IsBinary(char * s)
{
    int n = strlen(s);

    for (int i = 0; i < n; ++i)
    {
        if (s[i] != '0' && s[i] != '1')
        {
            return false;
        }
    }
    return true;
}

//Функция, переводящая строку в двоичное число, далее в десятичное
int binToDec(char * s)
{
    int n = strlen(s);
    int sum = 0;
    int deg = 1;
    int razr = 0;
    for (int i = n-1; i >= 0; --i)
    {
        razr = (int)(s[i]-48); //Переводим строку в двоичное число ("0" в таблице символ имеет номер "48", "1" -- номер "49")
        sum += razr*deg; //Поразрядно умножаем каждую цифру двоичного числа на 2^(разряд этой цифры)
        deg *= 2;
    }
    return sum;
}

int main()
{
    char s[32];
    cout << "Введите двоичное число" << endl;

    cin >> s;

    int num = 0;

    if (IsBinary(s) == 1)
    {
        num = binToDec(s);
        cout << num << endl;
    }
    else
    {
        cout << "Не является двоичным числом!" << endl;
    }

    return 0;
}

