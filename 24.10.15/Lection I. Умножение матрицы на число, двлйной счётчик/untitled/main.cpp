#include <iostream>
#include <ctime>

using namespace std;

//Вывод матрицы на экран
void printm(int m, int n, int ** matr)
{
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j< n; ++j)
        {
            cout << matr[i][j] << " ";
        }
        cout << endl;
    }
}

//Умножение матрицы на скалярное число
int ** matrxScalarMultiplication(int m, int n, int ** matr, int scalar)
{
    int ** matrMult = new int * [m];
    for (int i = 0; i < m; ++i)
    {
        matrMult[i] = new int [n];
        for (int j = 0; j < n; ++j)
        {
            matrMult[i][j] = matr[i][j]*scalar;
        }
    }
    return matrMult;
}

int main()
{
    cout << "Умножение матрицы на скалярное число:" << endl;
    srand(time(NULL));
    int m = 0, n = 0, scalar = 0;
    cin >> m >> n >> scalar;

    int ** matr = new int * [m];
    for (int i = 0; i < m; ++i)
    {
        matr[i] = new int [i];
        for (int j = 0; j < m; ++j)
        {
            matr[i][j] = rand()%10;
        }
    }

    printm(m, n, matr);
    cout << endl;

    int ** matrMult = matrxScalarMultiplication(m, n, matr, scalar);

    printm(m, n, matrMult);

    //Удаление массивов
    for (int i = 0; i < m; ++i)
    {
        delete[] matr[i];
        delete[] matrMult[i];
    }
    delete[] matr;
    delete[] matrMult;

    //Двойной счётчик
    cout << endl;
    cout << "Двойной счётчик:" << endl;
    for (int i = 0, j = 0; i < 5; ++i, j +=2)
    {
        cout << i << " " << j << endl;
    }

    return 0;
}

