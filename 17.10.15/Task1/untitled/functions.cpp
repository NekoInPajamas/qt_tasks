#include <iostream>
#include <ctime>
#include "functions.h"

using namespace std;


//Функция, генерирующая матрицу размерности (strg x colmn)
void matrxGeneration(int strg, int colmn, double ** matrx)
{
    for (int i = 0; i < strg; ++i)
    {
        for (int j = 0; j < colmn; ++j)
        {
            matrx[i][j] = rand()%10;
        }
    }
    }

//Функция, выводящая матрицу на экран
void printm(int strg, int colmn, double ** matrx)
{
    for (int i = 0; i < strg; ++i)
    {
        for (int j = 0; j < colmn; ++j)
        {
            cout << matrx[i][j] << " ";
        }
        cout << endl;
    }
cout << endl;
}

//Функция, перемножающая 2 матрицы
void matrixMultiplication(double ** matrix1, double ** matrix2, double ** matrix_Res, int k, int l, int n)
{
    double sum = 0;
    for (int i = 0; i < k; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            for (int p = 0; p < l; ++p)
            {
                sum += matrix1[i][p]*matrix2[p][j];
            }
            matrix_Res[i][j] = sum;
            sum = 0;
        }
    }
}
