/*
Напишите программу, генерирующую и перемножающую две матрицы. Ввод
размерностей и вывод результата должны осуществляться в командной строке.
*/

#include <iostream>
#include <ctime>
#include "functions.h"

using namespace std;

int main(int argc, char ** argv)
{
//    int k, l, m, n;
//    cout << "Введите размерность первой матрицы" << endl;
//    cin >> k >> l;
//    cout << "Введите размерность второй матрицы" << endl;
//    cin >> m >> n;

    //Конвертация аргументов командной строки в числа
    int k = atoi(argv[1]);
    int l = atoi(argv[2]);
    int m = atoi(argv[3]);
    int n = atoi(argv[4]);

    if (l != m)
    {
        cout << "Количество столбцов первой матрицы и количество строк второй матрицы должно совпадать!" << endl;
    }
    else
    {
        srand(time(NULL));

        //Создание динамических массивов для хранения матриц
        double ** matrx1 = new double * [k];
        double ** matrx2 = new double * [m];
        double ** matrx_Res = new double * [k];

        for (int i = 0; i < k; ++i)
        {
            matrx1[i] = new double [l];
            matrx_Res[i] = new double [n];
        }
        for (int i = 0; i < m; ++i)
        {
            matrx2[i] = new double [n];
        }

        //Генерирование матриц, вывод их на экран и их перемножение
        matrxGeneration(k, l, matrx1);
        matrxGeneration(m, n, matrx2);

        cout << endl;
        cout << "Первая матрица:" << endl;
        printm(k, l, matrx1);
        cout << "Вторая матрица:" << endl;
        printm(m, n, matrx2);

        matrixMultiplication(matrx1, matrx2, matrx_Res, k, l, n);
        cout << "Произведение матриц:" << endl;
        printm(k, n, matrx_Res);

        //Удаление всех матриц
        for (int i = 0; i < k; ++i)
        {
            delete[] matrx1[i];
            delete[] matrx_Res[i];
        }
        delete[] matrx1;
        delete[] matrx_Res;
        for (int i = 0; i < n; ++i)
        {
            delete[] matrx2[i];
        }
        delete[] matrx2;
    }

    return 0;
}

