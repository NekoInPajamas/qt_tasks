/*
Напишите программу, генерирующую и транспонирующую матрицу. Ввод
размерностей и вывод результата должны осуществляться в командной строке.
*/

#include <iostream>
#include <ctime>
#include "functions.h"

using namespace std;

int main(int argc, char ** argv)
{
//    int m, n;
//    cout << "Введите размерность матрицы" << endl;
//    cin >> m >> n;

    //Конвертация аргументов командной строки в числа
    int m = atoi(argv[1]);
    int n = atoi(argv[2]);

    srand(time(NULL));

    //Создание динамического массива для хранения матрицы
    double ** matrx = new double * [m];
    for (int i = 0; i < m; ++i)
    {
        matrx[i] = new double [n];
    }

    //Генерирование исходной матрицы и вывод её на экран
    matrixGeneration (m, n, matrx);
    cout << endl;
    cout << "Исходная матрица:" << endl;
    printm (m, n, matrx);

    //Создание динамического массива для хранения транспонированной матрицы
    double ** matrxTr = new double * [n];
    for (int i = 0; i < n; ++i)
    {
        matrxTr[i] = new double [m];
    }

    //Транспонирование исходной матрицы и вывод новой матрицы на экран
    transposition (m, n, matrx, matrxTr);
    cout << "Транспонированная матрица:" << endl;
    printm (n, m, matrxTr);

    //Удаление динамических массивов
    for (int i = 0; i < m; ++i)
    {
        delete[] matrx[i];
    }
    delete[] matrx;

    for (int i = 0; i < n; ++i)
    {
        delete[] matrxTr[i];
    }
    delete[] matrxTr;

    return 0;
}

