#include <iostream>
#include <ctime>
#include "functions.h"

using namespace std;

//Функция, генерирующая матрицу размерности m x n
void matrixGeneration(int m, int n, double ** matrix)
{
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            matrix[i][j] = rand()%10;
        }
    }
}

//Функция, выводящая матрицу на экран
void printm (int m, int n, double ** matrix)
{
    for (int i = 0; i < m; ++i)
    {
        for (int j = 0; j < n; ++j)
        {
            cout << matrix[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
}

//Транспонирование матрицы
void transposition(int m, int n, double ** matrix, double ** matrixTr)
{
    for (int j = 0; j < n; ++j)
    {
        for (int i = 0; i < m; ++i)
        {
            matrixTr[j][i] = matrix[i][j];
        }
    }
}

