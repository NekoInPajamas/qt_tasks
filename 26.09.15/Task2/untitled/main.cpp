/*
Для заданных значений n и x вычислить выражение:
S = sin x + sin sin x + ... + sin sin ...sin x.
*/

#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int n;
    double x;

    cout << "Введите n и x" << endl;
    cin >> n >> x;

    double sum = 0;

    for (int i = 0; i < n; ++i)
    {
        x = sin(x);
        sum += x;
    }

    cout << "S = " << sum << endl;
    return 0;
}

