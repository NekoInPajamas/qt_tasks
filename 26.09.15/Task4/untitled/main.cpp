/*
Вычислить и вывести на экран в виде таблицы значения функции, заданной
с помощью ряда на интервале 0<x<=2 с шагом dx с точностью epsilon.
*/

#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int epsilon;
    double dx, precision;

    cout << "Введите шаг и точность" << endl;
    cin >> dx >> epsilon;

    precision = pow(10,-epsilon);

double arg = 1;
double sum = 0;
int i = 0;

    for (double x = dx; x <= 2; x += dx)
    {
        while (abs(arg) >= precision)
        {
            arg = pow(-1,i)*pow(x-1,i+1)/(i+1);
            //cout << arg << endl;
            sum += arg;
            ++i;
        }
        cout << x << "\t" << sum << endl;
        sum = 0;
        i = 0;
        arg = 1;
    }


    return 0;
}

