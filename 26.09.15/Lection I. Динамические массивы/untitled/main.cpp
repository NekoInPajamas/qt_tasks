#include <iostream>
#include <ctime>

using namespace std;

int main()
{
    int n;

    //Определяется размер будущего массива
    cout << "Введите размер массива" << endl;
    cin >> n;

    //Инициализация генератора случайных чисел
    srand(time(NULL));

    //Выделяется память под n чисел типа int и в указатель записывается
    //адрес первой ячейки первого числа типа int
    int *x = new int [n];

    //В созданные ячейки типа int записываются сгенерированные
    //случайные числа от 0 до 9 и выводятся на экран
    for (int i = 0; i < n; i++)
    {
        x[i] = rand()%10; // x[i] == *(x+i)
        cout << x[i] << endl;
    }

    //Созданный массив удаляется
    delete[] x;

    //Определяются размеры будущего массива
    int k, m;

    cout << "Введите размеры массива" << endl;

    cin >> k >> m;

    //Выделяется память для массива из k указателей, в указатель y записывается
    //адрес первого элемента из массива
    int **y = new int *[k];

    //Для каждого i cоздаётся область из m чисел типа int, адрес первого элемента
    //первой ячейки области записывается в y[i] (т.е *(y+i)) (y -- указатель)
    for (int i = 0; i < k; ++i)
    {
        y[i] = new int [m]; // y[i] == *(y+i)
        for (int j = 0; j < m; ++j)
        {
            y[i][j] = rand()%10; //В j-ю ячейку области из m ячеек, адрес первой ячейки
                                 //которой хранится в i-й ячейке области из k ячеек,
                                 //записывается случайное число от 0 до 9
            cout << y[i][j] << endl;
        }

        //Удаляется область из m ячеек, адрес первой ячейки которой хранится в i-й ячейке области из k ячеек
        delete[] y[i];
    }

    //Удаляется массив указателей
    delete[] y;

    return 0;
}

