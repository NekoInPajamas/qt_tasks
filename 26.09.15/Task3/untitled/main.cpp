/*
Написать программу, которая вычисляет число Pi с заданной пользователем
точностью * . Для вычисления значения числа Pi воспользоваться формулой:
Pi = 4*(1 - 1/3 + 1/5 - 1/7 + 1/9 - ...).
* Под точностью в данном случае понимается абсолютная величина очередного
слагаемого.
*/

#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int n;

    cout << "Введите степень точности" << endl;
    cin >> n;

    double precision = pow(10,-n);

    double pi = 0;
    double arg = 1;
    int k = 0 ;

    while (abs(arg) >= precision)
    {
        arg = pow(-1.0,k)*(1.0/(2*k+1));
        //cout << arg << endl;
        pi += 4*arg;
        ++k;
    }

    cout << pi << endl;
    return 0;
}

