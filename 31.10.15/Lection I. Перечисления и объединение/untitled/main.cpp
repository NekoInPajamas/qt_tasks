#include <iostream>

using namespace std;

int main()
{
    enum DayOfWeek {Monday = 1, Tuesday, Wednesday, Friday = 5};
    cout << DayOfWeek(0) << endl;
    DayOfWeek w = Monday;
    cout << w << endl;
//    switch (w)
//    {
//        case Friday:
//        {
//            cout << "It's Friidaaay!" << endl;
//            break;
//        }
//    }
    DayOfWeek w1 = DayOfWeek(3);
    cout << w1 << endl;

    //Объединение (вместо 2х полей выбирается наибольшая ячейка; здесь sizeof(Test) = 8)
    union Test
    {
        double x;
        int y;
    };

    union RGB
    {
        char rgb[3];
        char * indexed; //#FF0022
    };

    RGB green;
    green.rgb[0] = 0;
    green.rgb[1] = 255;
    green.rgb[2] = 0;

    return 0;
}

