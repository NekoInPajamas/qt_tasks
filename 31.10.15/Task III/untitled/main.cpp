/*
Написать программу, которая считывает текст из файла и выводит на экран, заменив все строчные буквы (английского алфавита)
на прописные, прописные на строчные, а цифры от 0 до 9 на слова «zero», «one», ..., «nine».
*/

#include <iostream>
#include <fstream>
#include <cstring>

using namespace std;

int main()
{
    ifstream in;
    in.open("in.txt");

    if (!in.is_open())
    {
        cout << "Can't read the file!" << endl;
    }

    char fileText[128]; //Переменная для записи текущей строки
    int symbol; //Переменная для записи кода текущего символа

    while (!in.eof())
    {
        in.getline(fileText,128); //Считываем одну строку
        for (int i = 0; i < strlen(fileText); ++i)
        {
            symbol = fileText[i]; //Поочерёдно рассматриваем коды символов текущей строки
            if (symbol >=65 && symbol <=90) //Прописные буквы заменяем на строчные
            {
                fileText[i] = symbol + 32;
                cout << fileText[i];
            }
            else if (symbol >= 97 && symbol <= 122) //Строчные буквы заменяем на прописные
            {
                fileText[i] = symbol - 32;
                cout << fileText[i];
            }
            else switch (symbol) //Если встречается цифра, записываем её название буквенно
            {
                case 48: {cout << "zero"; break;}
                case 49: {cout << "one"; break;}
                case 50: {cout << "two"; break;}
                case 51: {cout << "three"; break;}
                case 52: {cout << "four"; break;}
                case 53: {cout << "five"; break;}
                case 54: {cout << "six"; break;}
                case 55: {cout << "seven"; break;}
                case 56: {cout << "eight"; break;}
                case 57: {cout << "nine"; break;}
                default: cout << fileText[i]; //Если символ -- не буква и не цифра, просто выводим на экран
            }
        }
        cout << endl;
    }

    in.close();

    return 0;
}

