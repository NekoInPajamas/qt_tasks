/*
Написать программу, которая считывает из входного файла матрицу, транспонирует ее и выводит отсортированную матрицу
в выходной файл. Замечание: в первой строке файла указывается размерность матрицы.
*/

#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    ifstream in;
    in.open("in.txt");

    if (!in.is_open())
    {
        cout << "Can't read the file!" << endl;
    }

    ofstream out;
    out.open("out.txt");

    if (!out.is_open())
    {
        cout << "Can't write into the file!" << endl;
    }

    int m, n;
    in >> m >> n; //Считываем размерности матрицы из первой строки файла

    //Считываем матрицу из файла, выводим на экран
    int ** arr = new int * [m];
    for (int i = 0; i < m; ++i)
    {
        arr[i] = new int [n];
        for (int j = 0; j < n; ++j)
        {
            in >> arr[i][j];
            cout << arr[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;


    //Транспонирование матрицы
    int ** arrTr = new int * [n];
    for (int i = 0; i < n; ++i)
    {
        arrTr[i] = new int [m];
        for (int j = 0; j < m; ++j)
        {
            arrTr[i][j] = arr[j][i];
            cout << arrTr[i][j] << " "; //Вывод на экран
            out << arrTr[i][j] << " "; //Вывод в файл
        }
        cout << endl;
        out << '\n';
    }

    in.close();
    out.close();

    return 0;
}

