#include <iostream>
#include <cmath>

using namespace std;

struct Point
{
    double x;
    double y;
    //Конструктор
    //Point(double x0 = 0, double y0 = 0): x(x0), y(y0) {}
    Point(double x0 = 0, double y0 = 0)
    {
        x = x0;
        y = y0;
    }
};

struct Polynom
{
    int degree;
    double * coef; //Динамический массив для коэффициентов полинома
    Polynom(int n, double * c)
    {
        degree = n;
        coef = c;
    }
};

double distance(Point p1, Point p2)
{
    return sqrt((p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y));
}

void printPoly(Polynom pol)
{
    cout << "Poly degree: " << pol.degree << endl;
    cout << "Poly coefs: ";
    for (int i = 0; i < pol.degree; ++i)
    {
        cout << pol.coef[i] << " ";
    }
    cout << endl;
}


int main()
{
    Point p0;
    cout << p0.x << " " << p0.y << endl;

    //Объявление структуры: вариант 1
    Point p;
    p.x = 10;
    p.y = -25;
    cout << p.x << " " << p.y << endl;

    //Объявление структуры: вариант 2
    Point p1 = {1,2};
    cout << p1.x << " " << p1.y << endl;

    //Указатель
    Point * pp = new Point();
    pp->x = 5;
    pp->y = 10;
    cout << pp->x << " " << pp->y << endl;

    //Полином
    int n = 3;
    double coef[3] = {1, 2, 3};
    Polynom poly(n,coef);
    printPoly(poly);

    //Расстояние между точками
    Point p2 = {5,10};
    Point p3 = {10,5};
    cout << "Расстояние между точками: " << distance(p2,p3) << endl;


    return 0;
}

