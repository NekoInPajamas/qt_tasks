/*
Написать программу, которая вычисляет среднее арифметическое чисел, находящихся в файле.
*/

#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

int main()
{
    ifstream in; //Инициализация потока для чтения
    in.open("in.txt"); //Открытие файла

    if (!in.is_open())
    {
        cout << "Can't open file!" << endl;
        return 1;
    }

    int nums = 20;
    int position = 10;

    int ** arr = new int * [nums]; //Матрица для хранения считываемых из файла чисел (каждое число занимает отдельную строку, т.е.
                                   //оно записано поразрядно, причём отдельный разряд -- в отдельном столбце
    for (int i = 0; i < nums; ++i)
    {
        arr[i] = new int [position];
    }

    int * razryads = new int [nums]; //Матрица для хранения количества разрядов каждого считываемого из файла числа

    char symbol;
    int amount = 0;
    int kol = 0;

    for (int i = 0; i < nums; ++i)
    {
        kol = 0;
        if (in.eof())
        {
            break;
        }
        else
        {
            for (int j = 0; j < position; ++j)
            {
                in.get(symbol); //Посимвольно считываем цифры из файла и записываем их как элементы массива
                if (symbol == ' ' || symbol == '\n' || in.eof())
                {
                    ++ amount;
                    //cout<<(int)symbol;
                    break;
                }
                else
                {
                    arr[i][j] = symbol - 48; //Преобразуем считываемый символ в число
                    ++kol;
                    cout << arr[i][j] << " ";
                }
            }
            cout << endl;
            razryads[i] = kol;
            //cout << razryads[i] << endl;
        }
    }
    cout << amount << endl;

    int sum = 0;
    int number = 0;
    int k;

    for (int i = 0; i < amount-1; ++i)
    {
        number = 0;
        k = razryads[i];
        for (int j = 0; j < k; ++j)
        {
            number += arr[i][j]*pow(10,k-1-j); //Считаем число, каждый разряд которого сохранён в столбцах текущей строки
        }
        sum += number;
    }

    cout << "Сумма чисел: " << sum << endl;
    cout << "Среднее арифметическое чисел: " << (double)sum/(amount-1) << endl;

    //Очистка памяти
    for (int i = 0; i < nums; ++i)
    {
        delete[] arr[i];
    }
    delete[] arr;

    //Закрытие файла
    in.close();

    return 0;
}

